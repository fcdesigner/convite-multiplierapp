<?php if ($editor) { ?>
	<link rel="stylesheet" href="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-summernote/summernote.css">
	<script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-summernote/summernote.min.js"></script>

	<script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/emojionearea-master/dist/emojionearea.js"></script>

	<script type="text/javascript">
		var ComponentsEditors = function () {
			var handleSummernote = function () {
				$('.summernote').summernote({height: 300});
			}
			
			return {
				//main function to initiate the module
				init: function () {
					handleSummernote();
				}
			};
		}();
		jQuery(document).ready(function() {    
			$("#facebook-title").emojioneArea();
			$("#facebook-description").emojioneArea();
			$(".hint2emoji").emojioneArea();
			ComponentsEditors.init(); 
		});
	</script>

<?php } ?>
<link href="../css/sonoclick.css" rel="stylesheet" type="text/css" />

<link href="../assets/global/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo HTTP_ROOT; ?>assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css">
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN THEME GLOBAL STYLES -->
<link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

<!-- END THEME GLOBAL STYLES -->

<style>

	.view_form_page .container{
		max-width: 1440px;
	}

	.view_form .social_row .fb{
		text-align: right;
		border-right:none;
	}

	.view_form .social_row .you_tube, .view_form .social_row .fb{
		min-height: 100px;
		display: flex;
		align-items: center;
		margin-bottom: 12px;
	}

	.view_form .social_row img {
		max-width: 180px;
	}

	.view_form .social_row .you_tube img {
		max-width: 165px;
	}

	.fb_public .fb_border {
		padding:0
	}

	.fb_public .fb_border>.col-sm-12{
		padding:0;
	}

	.fb_public .heading span{
		color:#f19300
	}

	.fb_public .next_btn{
		color: #000000;
		padding: 6px 32px;
		font-size: 16px;
		float: right;
		margin: 15px 20px 15px 0;
		background:#f0ff00;
		text-transform: uppercase;
		font-family: "Open Sans",sans-serif;
	}

	.fb_public .next_btn:hover{
		background:#f0ff00;
	}

	.fb_public .back_btn{
		color: #000000;
		padding: 6px 32px;
		font-size: 16px;
		float: right;
		margin: 15px 20px 15px 0;
		background:#ffa200;
		text-transform:uppercase;
		font-family: "Open Sans",sans-serif;
	}

	.fb_public .back_btn:hover{
		background:#ffa200;
	}

	.fb_public img{
		max-width:100%
	}

	.youtube_official{
		color: #ff0000 !important;
	}

	#stream_description-error, #stream_title-error
	{
		color: red;
	}

	#bottom-help-img{
		z-index: 2147483646;
		width: 60px;
		height: 60px;
		font-size: 18px;
		font-weight: 400;
		overflow: hidden;
		justify-content: center;
		align-items: center;
		display: flex;
		cursor: pointer;
		position: fixed;
		right: 10px;
		bottom: 10px;
		
	}

	.col-form-comming-users{
		padding: 0 30%;
		text-align: left;
	}

	/*iPhone 6/7/8 */
	@media (max-width: 768px){

		/* Form Comming Users */
		.col-form-comming-users{
			padding: 0 15%;
		}



	}

	.page-content{
		position: relative;
	}

	.page-content .view_form_page{
		background-image: url("http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2020/01/2303556048-BG.jpg");
		background-attachment: fixed !important;
		background-size: cover !important;
		background-position: top !important;
		background-repeat: no-repeat !important;
		position: relative;
	}

	<?php if ( $formInfo->selected_image ){ ?>

		.page-content .view_form_page{
			background-image: url(" <?=  $formInfo->selected_image ?> ");
		}

	<?php } ?>

	.page-content .view_form_page{
		margin-left: 0;
		border: 0;
		width: 100%;
	}

	.page-content-wrapper .page-content.view_form_page {
		margin-top: 0px;
	}

	.page-sidebar-closed .page-content-wrapper .page-content-wrapper .page-content.view_form_page{
		margin-left: 0 !important;
	}

	.img-zoom{
		transition: 0.5s transform ease;
	}

	.img-zoom:hover{
		transform: scale(1.1);
	}

	.btn-multiplier{
		color: black;
		border: 1px #00000036 solid;
		border-radius: 5px !important;
		padding: 5px 20px;
		transition: 0.3s ease background;
		cursor: pointer;
		margin: 5px 10px;
	}

	.btn-multiplier:hover{
		background: #fcc946 !important;
		color: white;
	}

	.btn-green{
		color: white;
		background: #5daaa7 !important;
	}

	/** Css Header */

	.page-wrapper > .tutorial-text-box{
		top: 40px;
	}


	#header_row{
		/* padding-bottom: 40px; */
		position: relative;
	}

	#header_logo_wrapper img{
		width: 50%;
	}

	#header_logo_wrapper .tutorial-text-box{
		top: 100px;
	}

	#flag_wrapper{
		position: absolute;
		right: 0;
		top: 50%;
		transform: translateY(-50%)
	}

	.view_form .flag_row{
		text-align: end;
		margin-right: 50px;
	}

	.view_form .flag_row .flag{
		text-decoration: none;
	}

	.view_form .flag_row .flag img{
		height: 40px;
		width: 45px;
		border-radius: 20px !important;
		margin: 0 10px;
	}

	.flag.active img{
		border: 2px solid #00ef00;
	}

	#background_edition{
		position: absolute;
		top: 10px;
		right: 0;
		z-index: 1;
	}

	#main_video{
		position: absolute;
		top: 0px;
		right: 0;
		z-index: 1;
	}

	/** Css Details Text */

	.row.detail_text{
		position: relative;
		background: none !important;
		margin-bottom: 15%;
	}

	#convite_text_wrapper{
		max-width: 100%;
		background: white;
		border-radius: 20px !important;
		padding: 50px;
		float: right;
		margin-bottom : 20px;
	}

	#convite_text_wrapper .tutorial-text-box{
		right: 30px;
	}

	#main_text{
		padding-bottom: 20px;
	}

	#main_text_edition_confirm{
		font-size: 1.5em;
		color: black;
		position: absolute;
		right: 0;
		top: 0;
		padding: 20px;
		cursor: pointer;
		transition: 0.5s ease all; 
	}

	#main_text_edition_confirm:hover{
		color: #fcc946
	}

	#invitation_headline_wrapper{
		position: absolute;
		left: 0;
		top: 50%;
		transform: translateY(-50%);
	}

	#invitation_headline_wrapper .tutorial-text-box{
		top: 50px;
	}

	#invitation_headline{
		color: white;
		font-weight: bold;
		text-shadow: 0px 0px 5px #000;
	}

	#invitation_headline > p{
		font-size: 1.5em !important;
		text-align: left;
		line-height: 1.2;
		margin-left: 50px;
	}

	#headline_edition_input{
		background: none;
		border: none;
		box-shadow: 0px 1px 0px 0px #ffffff63;
		width: 100%;
		color: white;
		text-shadow: 0px 0px 5px #000;
	}

	#headline_edition_confirm{
		font-size: 1.5em;
		color: white;
		position: absolute;
		right: 30px;
		transform: translate(50%, 50%);
		cursor: pointer;
		transition: 0.5s ease all; 
		text-shadow: 0px 0px 5px #000;
	}

	#headline_edition_confirm:hover{
		color: #fcc946;
	}

	#social_title_wraper{
		position: relative;
		min-height: 80px;
	}

	#social_title_edition_confirm{
		font-size: 1.5em;
		color: black;
		position: absolute;
		right: 0;
		transform: translate(50%, 50%);
		cursor: pointer;
		transition: 0.5s ease all; 
	}

	#social_title_edition_confirm:hover{
		color: #fcc946;
	}

	#social_title_edition_input{
		background: none;
		border: none;
		box-shadow: 0px 1px 0px 0px #00000070;
		width: 100%;
		color: black;
	}

	#initial-video{
		position: relative;
		width: 100%;
		display: inline-block;
	}

	/** Css Social Row */

	.view_form .social_row{
		border: none;
		background: white;
		border-radius: 20px !important;
		padding: 70px 100px 20px 100px;
		margin-bottom: 15%;
		clear: both;
	}

	.social_row .tutorial-text-box{
		right: 50%;
		top: 50%;
		transform: translate(50%, -50%);
	}

	#social_title{
		color: black;
		font-weight: bold;
	}

	#social_title > p{
		font-size: 1.5em !important;
		text-align: center;
		line-height: 1.2;
	}

	#social_title > p > b{
		font-weight: 600;
	}

	#social_warning{
		border: 1.5px solid #de0000;
		border-radius: 20px !important;
		padding: 20px;
		letter-spacing: 0.5px;
		line-height: 1.7;
		margin-bottom: 100px;
		text-align: left;
	}

	#col_social_text{
		padding-left: 0;
	}

	.col-form-comming-users{
		padding: 0;
	}

	.social_row > div{
		padding: 0 30px;
	}

	#social_image{
		margin-left: -30px;
		max-width: 250px;
	}

	.choose_one p{
		text-align: left;
		font-weight: 500;
		line-height: 1.5;
		letter-spacing: 0.5px;
	}

	.logo_row hr{
		margin: 40px 0;
	}

	#share_text{
		float: left;
		margin-left: 60px;
	}

	#privacy_text_edition_confirm{
		font-size: 1.5em;
		color: black;
		position: absolute;
		right: 0;
		bottom: 50%;
		transform: translate(50%, 50%);
		cursor: pointer;
		transition: 0.5s ease all;
	}

	#privacy_text_edition_confirm:hover{
		color: #fcc946;
	}

	#countryflag{
		padding-top: 0px;
		border: 1px solid #00000036;
	}

	.button-preview-copy{
		float: right;
		margin-right: 85px;
	}

	.you_tube{
		padding-left: 20px;
	}

	.view_form .social_row .fb_border img{
		max-width: 100%;
	}

	/** Css Footer */

	.view_form{
		padding: 0;
	}

	#footer_card{
		border: none;
		background: white;
		border-radius: 20px 20px 0 0 !important;
		padding: 50px 100px 10px 100px;
	}

	#footer_logo{
		width: 55%;
	}

	.footer-tag-img{
		width: 35%;
		vertical-align: middle;
		padding: 0 2%;
	}

	.footer-tag-img{
		text-decoration: none;
	}

	#footer_wrapper{
		position: relative; 
		margin-bottom: 20px;
	}

	#footer_login_wrapper{
		text-align: end;
		margin-top: 35px;
	}

	#footer_login_wrapper a:hover{
		color: white;
	}

	#footer_login_wrapper .btn{
		margin: 0 20px;
	}

	.knowMore_row{
		margin: 20px 0 40px 0;
	}

	#app_stores_wrapper{
		margin-bottom: 25px;
	}

	#app_stores{
		display: inline-block;
		width: 50%;
	}

	#text_rights{
		margin:0;
	}

	.knowMore_row a{
		color: black;
	}

	.knowMore_row a:hover{
		color: #5daaa7
	}

	#icons{
		text-align: end;
		margin-top: 20px;
	}

	#icons a{
		color: black;
		text-decoration: none;
	}

	#icons i{
		transition: 0.5s ease color;
		color: black;
		padding: 0 10px;
		font-size: 2em;
	}

	#icons i:hover{
		color: #fcc946;
	}

	/** Css Tutorial */

	.blocked{
		z-index: 0 !important;
	}

	.tutorial-overlay{
		position: fixed;
		width: 100%;
		top: 0;
		bottom: 0;
		left: 0;
		background: black;
		z-index: 1;
		opacity: 0.65;
	}

	.tutorial-item-active{
		position: relative;
		z-index: 2;
	}

	.tutorial-overlay.tutorial-item-active{
		opacity: 0;
	}

	.tutorial-start-text-box{
		min-width: 220px;
		background: white;
		position: fixed;
		top: 150px;
		right: 50%;
		padding: 10px;
		border-radius: 5px !important;
		z-index: 4;
		transform: translate(50%, 50%);
		text-align: center;
		border: 1px solid #00000059;
		width: 60%;
	}

	.tutorial-start-text-box h3{
		font-weight: bold;
	}

	.tutorial-start-text-box p{
		padding: 0 10px;
	}

	.tutorial-text-box{
		background: white;
		position: absolute;
		right: 0;
		top: 30px;
		padding: 15px !important;
		border-radius: 5px !important;
		margin-top: 15px;
		border: 1px solid #00000059;
		text-align: center;
		z-index: 2;
	}

	.tutorial-text-box h3{
		font-weight: bold;
	}

	.tutorial-button-wraper{
		text-align: center;
	}

	.icon-editable{
		position: absolute;
		top: 0;
		right: 0;
		font-size: 1.2em;
		padding: 20px;
		cursor: pointer;
		transition: 0.3s ease all;
	}

	.icon-editable.flags{
		top: -10px;
		right: -50px;
	}

	.icon-editable:hover{
		color: #fcc946;
	}

	#remove_video{
		position: absolute;
		top: 0px;
		left: 0;
		z-index: 1;
    	right: unset;
	}

	.lateral-padding-0{
		padding-left: 0;
		padding-right: 0;
	}

	.icon-white{
		color: white;
		text-shadow: 0px 0px 5px #000;
	}

	.icon-black{
		color: black;
	}

	.editable-content{
		transition: 0.5s all ease;
	}

	.editable-content-highlight{
		transform: scale(1.1);
	}

	/** Image Select */

	#images_preview .panel-collapse .panel-body{
		max-height: 250px; 
    	overflow: auto;
		min-height: 160px;
	}

	#images_modal{
		background: white;
		position: fixed;
		top: 100px;
		right: 50%;
		padding: 10px;
		border-radius: 5px !important;
		z-index: 100;
		transform: translate(50%, 0%);
		width: 80%;
		height: 760px;
	}

	#images_modal_video{
		background: white;
		position: fixed;
		top: 100px;
		right: 50%;
		padding: 10px;
		border-radius: 5px !important;
		z-index: 2;
		transform: translate(50%, 0%);
		width: 80%;
		height: 760px;
	}

	#close_modal_images{
		position: absolute;
		right: 10px;
		top: 10px;
		font-size: 1.5em;
		cursor: pointer;
		z-index: 2;
	}

	#close_modal_images:hover{
		color: #fcc946;
	}

	#close_modal_images_video{
		position: absolute;
		right: 10px;
		top: 10px;
		font-size: 1.5em;
		cursor: pointer;
		z-index: 2;
	}

	#close_modal_images_video:hover{
		color: #fcc946;
	}

	#images_wrapper{
		height: 700px;
		padding: 10px;
	}

	#images_wrapper_video{
		height: 700px;
		padding: 10px;
	}

	#images_preview{
		position: absolute;
		top: 0;
		left: 0;
		overflow: auto;
	}

	#images_preview_video{
		position: absolute;
		top: 0;
		left: 0;
		overflow: auto;
	}

	#image_show_wrapper{
		position: absolute;
		right: 0;
		top: 0;
		bottom: 0;
		box-shadow: -1px 0px 20px #99999954;
		overflow: auto;
	}

	.select-image-item{
		position: relative;
		width: 31%;
		box-shadow: 2px 5px 5px #99999982;
		float: left;
		margin: 10px 5px;
		cursor: pointer;
	}

	.add-file-wrapper > div{
		width: 100%;
		height: 110px;
		background-color: #5daaa7;
		transition: 0.5s ease all;
	}

	.add-file-wrapper > div:hover{
		background-color: #fccd56;
	}

	.add-file{
		font-size: 3.2em;
		position: absolute;
		top: 40%;
		left: 50%;
		transform: translate(-50%);
		color: white;
	}

	.select-image-item{
		height: 90px;
		overflow: hidden;
	}

	.select-image-item img{
		width: 100%;
		height: 100%;
	}

	.select-image-item video{
		width: auto;
		height: 100%;
	}

	.delete-image-icon{
		position: absolute;
		right: 5px;
		top: 5px;
		color: white;
		cursor: pointer;
		text-shadow: 0px 0px 5px #000;
		z-index: 1;
	}

	#image_holder_wrapper{
		width: 100%;
		padding: 0 20px;
		overflow: hidden;
	}

	#image_holder_wrapper img{
		height: 60vh;
	}

	#image_holder_wrapper > div{
		max-height: 60vh
	}

	#image_holder{
		width: 100%;
	}

	#logo_holder{
		width: 150px;
		height: auto !important;

		position: absolute;
		right: 50%;
		top: 50%;
		transform: translate(50%, -50%);
	}

	#colorpicker{
		height: 90px;
	}

	#video_holder_wrapper{
		width: 100%;
		padding: 0 20px;
		overflow: hidden;
	}

	#video_holder_wrapper video{
		height: 60vh;
	}

	#video_holder_wrapper > div{
		max-height: 60vh
	}

	#preview_video_holder{
		width: 100%;
	}

	#initial_video_placeholder{
		width: 100%;
		border: 2px dashed #5daaa7;
		height: 230px;
		border-radius: 5px !important;
	}

	#initial_video_placeholder i{
		position: absolute;
		top: 50%;
		right: 50%;
		transform: translate(50%, -50%);
		color: #5daaa7;
		font-size: 4em;
	}

	#right_images_controls{
		position: absolute;
		right: 10px;
		z-index: 2;
		display: inline-table;
	}

	#form_functions{
		position: fixed;
		right: 30px;
		bottom: 70px;
		z-index: 99;
	}

	#form_functions > div.img-zoom {
		position: relative;
		display: block;
		border-radius: 15px !important;
		background-color: #5daaa7;
		margin: 10px 0;
		cursor: pointer;
	}

	#form_functions .tutorial-text-box{
		right: 60px;
		top: 0;
	}

	#form_functions i {
		font-size: 1.2em;
		padding: 15px;
		color: white;
	}

	#submit_button i{
		margin-left: 3px;
	}

	#tutorial_button i{
		margin-left: 6px;
	}

	#form_functions > div.img-zoom:hover {
		background-color: #fcc946;
	}

	#form_functions > div.on {
		background-color: #fcc946;
	}

	#bottom-help-img{
		display: none !important;
	}

	#close_error_message{
		color: #ff0000bf;
		text-align: center;
		font-size: 4em;
		margin-top: 30px;
	}

	#info_message{
		color: #5daaa7;
		text-align: center;
		font-size: 4em;
		margin-top: 30px; 
	}

	#confirm_choice{
		margin: 0;
	}

	/** Css Layou Admin*/

	.page-header{
		transition: 0.5s ease all;
	}

	.page-header.preview{
		margin-top: -50px;
	}

	.page-sidebar-wrapper{
		position: fixed;
		width: 235px;
	}

	.page-sidebar{
		width: 100%;
	}

	.page-content-wrapper .page-content{
		overflow: auto;
		padding: 0px !important;
		transition: 0.5s ease all;
	}

	.page-footer{
		display: none;
	}

	.page-content.preview{
		margin-left: 0;
		border: 0;
		margin-top: -20px;
	}

	.page-container.preview{
		margin-top: 0;
		position: absolute;
		left: 0;
		right: 0;
	}

	#new_image i{
		display: none;
	}

	/** Css Layou Admin*/

	.page-header{
		transition: 0.5s ease all;
	}

	.page-header.preview{
		margin-top: -50px;
	}

	.page-sidebar-wrapper{
		position: fixed;
		width: 235px;
	}

	.page-sidebar{
		width: 100%;
	}

	.page-content-wrapper .page-content{
		overflow: auto;
		padding: 0px !important;
		transition: 0.5s ease all;
	}

	.page-footer{
		display: none;
	}

	.page-container.preview{
		margin-top: 0;
		position: absolute;
		left: 0;
		right: 0;
	}

	#new_image i, #new_video i{
		display: none;
	}

	#solid_color_input_wrapper{
		position: relative;
	}

	#solid_color{
		border: none;
	}

	#add_solid_color{
		position: absolute;
		right: 5px;
    	top: 10px;
		color: #58a4a0;
		z-index: 5;
		font-size: 1.5em;
		cursor: pointer;
		transition: 0.5s ease color;
	}

	#add_solid_color:hover{
		color: #fccf5a;
	}

	.input-group.color .input-group-btn i {
		position: absolute;
		display: block;
		cursor: pointer;
		width: 100%;
		height: 100%;
		right: 0;
		top: 0;
	}

	.btn.default{
		width: 100%;
		height: 100%;
	}

	/** Hidding buttons that are not working in the note editor */
	.note-editor .btn-fullscreen, .note-editor .dropdown-toggle, .note-editor .note-current-color-button{
		display: none;
	}

	.hide-content.preview{
		display: none !important;
	}

	/** Accordion style */

	.panel.panel-default{
		border: none;
		border-bottom: 1px solid #0000002b;
	}

	.panel-heading{
		background-color: #0000 !important;
	}

	a.accordion-toggle{
		color: #fcc946;
		transition: color ease 0.5s;
	}

	a.accordion-toggle.collapsed{
		color: black;
	}

	/** Share Modal Style */

	#share_modal{
		position: fixed;
		padding: 50px;
		background-color: white;
		left: 50%;
		transform: translate(-50%, -50%);
		top: 50%;
		z-index: 100;
		border-radius: 10px !important;
		box-shadow: 5px 5px 16px 2px #00000085;
	}

	#close_share_modal{
		position: absolute;
		top: 10px;
		right: 10px;
		font-size: 1.5em;
		cursor: pointer;
	}

	#share_modal > h2{
		text-align: center;
		margin-bottom: 0;
		font-weight: bold;
	}

	#share_modal > p{
		font-size: 1.15em;
		margin-top: 0;
    	text-align: center;
		color: #4d6ea7;
	}

	#share_card_row{
	    display: flex;
    	flex-wrap: wrap;	
		justify-content: space-between;
		align-content: center;
		margin: 35px 50px;
		text-align: -webkit-center;
		margin-top: 70px;
	}

	.share-card-wrapper{
		flex: auto;
	}

	.share-card{
		width: 200px;
		text-align: center;
		background-color: #f5f5f5bd;
		border: 1px solid #0000005c;
    	border-radius: 10px !important;
		transition: 0.3s ease background;
		cursor: pointer;
	}

	.share-card-icon {
		width: 100%;
		height: 100%;
		padding: 35px 55px;
		fill: #4d6ea7;
    	padding-bottom: 5px;
		transition: 0.3s ease fill;
	}

	.share-card > p {
		margin-top: 5px;
		font-size: 1.2em;
	}	

	.share-card:hover{
		background: #4d6ea7;
	}

	.share-card:hover > p{
		color: #ffffff;
	}

	.share-card:hover .share-card-icon{
		fill: #ffffff;
	}

	#share_footer_wrapper{
		display: flex;
    	flex-wrap: wrap;	
		justify-content: start;	
		margin: 10px 50px;
	}

	#share_footer_wrapper > div{
		flex: 1;
	}

	#share_footer_img_wrapper img{
		max-width: 400px;
		padding: 0px 30px;
		border: 1px solid #00000047;
		border-radius: 10px !important;
		margin-right: 25px;
	}

	#share_facebook_icon{
		position: absolute;
		right: 10px;
		bottom: 30px;
		font-size: 3.5em;
		color: #4d6ea7;
	}

	@media (max-width: 1680px){

		.container{
			width: 1200px;
		}

		.select-image-item{
			width: 30%;
		}

		.select-video-item{
			width: 30%;
		}
	}

	@media (max-width: 1500px){

		.container{
			width: 1100px;
		}

		.select-image-item, .select-video-item{
			width: 30%;
		}

		#colorpicker{
			height: 70px;
		}

	}

	@media (max-width: 1300px){

		#images_controls .btn{
			margin: 0;
		}

		.select-image-item, .select-video-item{
			width: 29%;
		}

	}

	@media (max-width: 1100px){

		.container{
			width: 750px;
		}

		.preview .container{
			width: 970px;
		}

		.button-preview-copy{
			margin: 0;
		}

		#app_stores{
			width: 70%;
		}

		.icon-editable.flags {
			right: 0px;
		}

		.select-image-item {
			width: 45%;
		}

		.select-video-item {
			width: 45%;
		}

		#headline_edition_confirm{
			right: 30px;
		}

		#social_warning{
			margin-bottom: 50px;
		}

		#footer_login_wrapper{
			text-align: center;
		}

		#share_text{
			margin-left: 0;
		}

		#footer_card{
			padding: 50px 50px 10px 50px;
		}

		.preview #footer_card{
			padding: 50px 100px 10px 100px;
		}

		#footer_login_wrapper{
			width: 50%;
		}

		.view_form .social_row .you_tube, .view_form .social_row .fb{
			width: 100%;
			min-height: inherit;
		}

		.you_tube{
			padding-right: 35px;
		}

		#footer_login_wrapper .btn{
			margin: 0 10px;
			font-size: 11px;
		}

		#icons{
			width: 50%;
		}

		#icons i{
			font-size: 1.8em;
		}

		#new_image, #new_video{
			border: none;
		}

		#new_image span, #new_video span{
			display: none;
		}

		#new_image i, #new_video i{
			display: inherit;
			font-size: 1.5em;
			padding-top: 5px;
		}
	}

	@media (max-width: 992px){
		
		.container{
			width: 900px;
		}

		.page-content-wrapper .page-content.preview{
			margin-top: -10px !important;
		}

		.select-image-item{
			width: 48%;
		}

		.select-video-item{
			width: 48%;
		}

		.preview #footer_card {
			padding: 20px;
		}
	}

	@media (max-width: 768px){

		.container{
			width: 700px;
		}

		.preview .container{
			width: 700px;
		}

		.preview #footer_card {
			padding: 20px;
		}

		#flag_wrapper{
			top: 15px;
			transform: translateX(-50%);
		}

		#header_logo_wrapper{
			margin-top: 50px;
		}

		#header_logo_edition{
			top: 60px;
		}

		#convite_text_wrapper{
			display: inline-block;
		}

		#invitation_headline_wrapper{
			position: relative;
			right: inherit;
			top: inherit;
			transform: unset;  
			margin: 40px 0;
		}

		.view_form .social_row .you_tube, .view_form .social_row .fb {
			width: 50%;
			min-height: 100px;
		}

		#invitation_headline p {
			text-align: center;
			margin-left: 0px;
		}

		#social_warning{
			margin-bottom: 50px;
		}

		#footer_login_wrapper{
			text-align: center;
			width: 100%;
		}

		#share_text{
			margin-left: 0;
		}

		iframe{
			margin-bottom: 20px;
		}

		#icons{
			width: 100%;
			text-align: center;
			margin-top: 25px;
		}

		#new_image, #new_video{
			padding: 10px;
		}

		#crop_image{
			display: none;
		}

		#app_stores{
			width: 80%;
		}

		.select-image-item{
			width: 100%;
			height: 110px;
		}

		.select-video-item{
			width: 100%;
		}

		.tutorial-text-box {
			top: 25px;
		}
	}

	@media (max-width: 480px){
		
		.btn-multiplier{
			margin: 5px 0;
		}

		.container{
			width: 320px;
		}

		.preview .container{
			width: 320px;
		}

		.preview #footer_card {
			padding: 20px;
		}

		#images_preview{
			width: 100%;
		}

		#image_show_wrapper{
			width: 0;
    		height: 0;
		}

		#images_preview .panel-collapse .panel-body{
			max-height: 180px; 
		}

		#flag_wrapper{
			top: 15px;
			transform: none;
		}

		#header_logo_wrapper{
			margin-top: 100px;
		}

		#convite_text_wrapper{
			padding: 20px;
		}

		#invitation_headline_wrapper{
			position: relative;
			right: none;
			top: none;
			transform: none;  
		}

		#invitation_headline p {
			text-align: center;
			margin-left: 0px;
		}

		.view_form .social_row{
			padding: 25px;
		}

		.choose_one{
			padding: 0;
		}

		#col_social_text{
			padding: 0;
		}

		#col_social_form{
			padding: 10px;
		}

		.view_form .social_row .you_tube, .view_form .social_row .fb {
			width: 50%;
			min-height: inherit;
		}

		.you_tube{
			padding: 10px 0px 0 40px;
		}

		.fb-login{
			padding: 0;
			padding-left: 35px;
		}

		#social_warning{
			font-size: 0.95em;
			padding: 20px;
			line-height: 1.5;
			margin-bottom: 15px;
		}

		#social_warning p {
			margin: 0;
		}

		#social_image {
			margin-left: -20px;
		    max-width: 220px;
		}

		.form-group .row > div{
			margin-bottom: 15px;
		}

		.row.logo_row{
			padding: 0;
		}

		.logo_row hr{
			margin: 20px 0;
		}

		#share_text{
			float: none;
		}

		.button-preview-copy{
			float: none;
		}

		#footer_card{
			padding: 20px;
		}

		#footer_logo{
			width: 90%;
		}

		#footer_login_wrapper .btn {
			margin: 0px 5px;
			font-size: 13px;
		}

		.footer-tag-img {
			width: 45%;
		}

		iframe{
			height: 200px !important;
		}

		.knowMore_row{
			margin: 0px 0 40px 0; 
		}

		#app_stores{
			width: 100%;
		}
		
		#icons{
			padding: 0;
		}

		.delete-image-icon {
			right: unset;
    		left: 5px;
		}
	}

	@media (max-height: 800px){
		#images_preview .panel-collapse .panel-body{
			max-height: 200px; 
		}
	}

</style>

<style>

	/** NEW MAIN TEXT STYLE */


	/** MAIN STYLE */

	.add-new{
		width: 100%;
		height: 100%;
		display: flex;
		flex-wrap: wrap;
		position: relative;
	}

	.add-new .icon-wrapper{
		position: absolute;
		top: 50%;
		right: 50%;
		transform: translate(50%, -50%);
		transition: scale ease 0.5s;
	}

	.add-new .icon-wrapper .add-icon{
		color: #5daaa7;
		font-size: 3em;
		cursor: pointer;
	}

	.add-new .add-icon:hover{
		transform: scale(1.15);
		color: #fcc946;
	}

	.add-new .add-new-divider{
		flex: auto;
		position: relative;
		margin: 0px 25px;
	}

	/** COLUMN FUNCTION STYLE */

	.content-column .column-functions-wrapper{
		position: absolute;
		top: 0;
		right: 50%;
		transform: translate(50%, 0);
		background-color: #5daaa7;
		width: 100px;
		height: 25px;
		z-index: 1;
		transition: opacity ease 0.3s;
	}

	.content-column .column-functions-wrapper.hide{
		opacity: 0;
		display: block !important;
	}

	.content-column .column-functions-wrapper .column-function-icon{
		color: white;
		font-size: 17px;
		margin: 5px 5px;
	}

	.content-column .column-functions-wrapper .column-function-icon:hover{
		color: #fcc946;
		transform: scale(1.5);
		cursor: pointer;
	}

	/** NEW COLUMN STYLE */

	.add-new-column .add-icon{
		transform: rotate(90deg);
	}

	.add-new-column .add-icon:hover{
		transform: scale(1.15) rotate(90deg);
		color: #fcc946;
	}

	/** NEW CONTENT STYLE */

	.new-content-container{
		width: 100%;
		display: flex;
		flex-wrap: wrap;
	}

	.new-content-container .content-column, .add-new-column{
		width: 100%;
		border: 1.2px solid #5daaa7;
		height: 230px;
		border-radius: 5px !important;
		margin: 10px 2.5px;
		overflow: hidden;
	}

	.new-content-container .content-column .add-new-content-wrapper{
		height: 100%;
	}

	.new-content-container .content-column .text-content{
		margin: 10px;
		text-align: left;
		word-break: break-word;
	}

	.new-content-container .content-column .midia-content{
		margin: 10px;
		max-height: 100%;
		max-width: 100%;
	}

	.new-content-container .content-column .midia-content video{
		max-height: 100%;
		max-width: 100%;
	}

	/** CKEDITOR STYLE */

	.cke{
		margin: 20px 10px 0px 10px;
	}

	/** PREVIEW STYLE */

	.add-new-column.preview{
		display: none;
	}

	.new-content-container.preview > div{
		display: none;
	}

	.new-content-container.preview .content-column.chosen-column.preview{
		display: block;
	}

	.new-content-container.preview .content-column.preview{
		border: 0;
	}

	.new-content-container.preview .content-column.preview > div{
		display: none;
	}

	.new-content-container.preview .content-column.preview .chosen-content{
		display: block;
	}

</style>

<?php

	$form_data = json_decode( $FormListingData['form_body']);
	$form_data = json_decode( $form_data, true);
	$session = $this->request->session();
	$themeFa =  '<i class="fa fa-gift"></i>';
	if($session->read( 'social.is_login' ) == "google"){
	$themeFa = '<i class="fa fa-youtube"></i>';
	$theme = $session->read('social.theme');
		}
	$is_success_wizard=false;

	$staticimagepath = S3_MEDIA_PATH_ELECTRONICS_STATIC_IMAGES;

	$staticvideopath = S3_MEDIA_PATH_ELECTRONICS_STATIC_VIDEO;
	$dynamicvideopath = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO;
	$formingpath = S3_MEDIA_PATH_ELECTRONICS_USER_IMAGES;

?>

<link rel = "stylesheet" href = "https://fonts.googleapis.com/css?family=Roboto&display=swap" />

<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />

<form role="form" class="form-horizontal" method="POST" id="edition_form">

	<input class = "hide" value="<?php if(!empty($formId)){echo $formId;} ?>" name="form_id" id="form_id">
	<input class = "hide" id = "background_edition_input" type = "file" accept = "image/*">
	<input class = "hide" id = "chosen_background_image_base_64" name = "chosen_background_image_base_64">

	<input class = "hide" id = "no_video" name = "no_video"> 
	<input class = "hide" id = "chosen_video" name = "chosen_video">
	<input class = "hide" id = "chosen_video_id" name = "chosen_video_id">
	<input class = "hide" id = "main_text_html" name = "main_text_html">

	<input class = "hide" id = "chosen_image" name = "chosen_image">

	<div class="page-wrapper">

		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container_dummy editable-content tutorial-step" data-tutorial-show-parent = false data-tutorial-name = " <?= __(backgroundImageTitle) ?>" data-tutorial-text = "<?= __(defaultEditionText) ?>">
			
			<i class="fas fa-edit icon-editable icon-white tooltip-tippy" id = "background_edition" data-tippy-content = "<?= __(backgroundTooltip) ?>"></i>

			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content view_form_page">

					<div class="container">

						<div class="row view_form">

							<div class="col-sm-12">

								<?php 
									$selectedLanguage='Portuguese';
									$gifImage='';
									if($session->read('language') == "en_US"){
										$selectedLanguage='English';
										$url = "../img/facebook_login.png";
										$yurl = "../img/you_tube.png";
										$gifImage = "../img/public_en.gif";
										$form_content = $formInfo->form_description;

									}else if($session->read('language') == "en_SP"){
										$selectedLanguage='Spanish';
										$url = "../img/facebook_login.png";
										$yurl = "../img/you_tube.png";
										$gifImage = "../img/public_es.gif";
										$form_content = $formInfo->form_description_sp;

									}else{
										$selectedLanguage='Portuguese';
										$url = "../img/facebook_login.png";
										$yurl = "../img/you_tube.png";
										$gifImage = "../img/public_pt.gif";
										$form_content = $formInfo->form_description_pt;

									}     

									$form_content = ($form_content ) ? $form_content : "<p id = 'cause_description_text'>". __(causeDescritionText) ."</p>";
									$headline = ($form_title_lang ) ? $form_title_lang : __(causeTitle);
								?>

								<canvas width = 500 height = 500 class = "hide" id = "canvas"></canvas>
					
								<div class = "row" id = "header_row">

									<div class = "col-md-3" id = "header_logo_wrapper">
										<img class = "img-zoom editable-content tutorial-step" data-tutorial-name = "<?= __(Logo) ?>" data-tutorial-text = "<?= __(defaultEditionText) ?>" id = "header_logo" src = "<?= $formInfo->selected_header_image; ?>">
										<i class="fas fa-edit icon-editable icon-white tooltip-tippy" id = "header_logo_edition" data-tippy-content = "<?= __(logoTooltip) ?>" ></i>

										<input class = "hide edition-input" id = "header_logo_edition_input" type = "file" accept = "image/*">
										<input class = "hide" id = "chosen_header_logo_image_base_64" name = "chosen_header_logo_image_base_64">
									</div>

									<div class = "col-md-9"  id = "flag_wrapper">

										<div class = "row flag_row tutorial-step" data-tutorial-name = "<?= __(languageTitle) ?>" data-tutorial-text = "<?= __(languageText) ?>" >

											<div class = "editable-content"> 
												
												<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=2" class="flag <?php echo ($session->read('language') == "en_BR")?'active':''; ?>" >
													<img class = "img-zoom" src="<?php echo '../img/brasilflag.jpg' ; ?>" / >
												</a> 

												<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=1" class="flag <?php echo ($session->read('language') == "en_US")?'active':''; ?>">
													<img class = "img-zoom" src="<?php echo '../img/usaflag.jpg' ; ?>" / >
												</a>

												<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=3" class="flag <?php echo ($session->read('language') == "en_SP")?'active':''; ?>">
													<img class = "img-zoom" src="<?php echo '../img/spanish.jpg' ; ?>" / > 
												</a> 

												<input class = "hide edition-input" id = "language_edition_input" name = "language" value = "<?= $selectedLanguage ?>">

											</div>

											<i class="fas fa-edit icon-editable flags icon-white tooltip-tippy hide next-release" data-tippy-content = "<?= __(languageTooltip) ?>"></i>

										</div>
							
									</div>

								</div>
					
								<div class="row detail_text">

									<div class = "col-md-5" id = "invitation_headline_wrapper">
										<h2 class = "editable-content tutorial-step" 
											data-tutorial-name = "<?= __(causeTitle) ?>" 
											data-tutorial-text = "<?= __(defaultEditionText) ?>"
											id = "invitation_headline"><?= $headline ?> </h2>
										<i class="fas fa-edit icon-editable icon-white tooltip-tippy" id = "headline_edition_button" data-tippy-content = "<?= __(causeNameTooltip) ?>"></i>
										<i class="far fa-check-circle hide img-zoom tooltip-tippy" id = "headline_edition_confirm" data-tippy-content = "<?= __(confirmEdition) ?>"></i>
										<textarea class = "hide edition-input" id = "headline_edition_input" type = "text" name = "headline"></textarea>
									</div>

									<div class = "col-md-7" id = "convite_text_wrapper">

										<div class = "new-content-container">	
											<?= $form_content ?>
										</div>

										<div class = "add-new add-new-column">

											<div class = "add-new-divider">
												<div class= "icon-wrapper">
													<i class="fas fa-minus add-icon add-content-icon add-one-column tooltip-tippy" data-tippy-content = "<?= __(addOneColumn) ?>"></i>
												</div>
											</div>

											<div class = "add-new-divider">
												<div class= "icon-wrapper">
													<i class="fas fa-equals add-icon add-content-icon add-two-column tooltip-tippy" data-tippy-content = "<?= __(addTwoColumns) ?>"></i>
												</div>
											</div>

											<div class = "add-new-divider">
												<div class= "icon-wrapper">
													<i class="fas fa-bars add-icon add-content-icon add-three-column tooltip-tippy" data-tippy-content = "<?= __(addThreeColumns) ?>"></i>
												</div>
											</div>

										</div>

									</div>
								
								</div>

								<div class="row social_row ">

								<div class = "col-md-6" id = "col_social_text">

									<div class = "col-md-12">
										<img class = "img-zoom" id = "social_image" src = "https://multiplierapp.com.br/app/img/logo_big.jpg">
									</div>

									<div class="col-sm-12 choose_one">
										<p id = "privacy_text" > <?= __(defaultPrivacyText) ?> <br> <?= __(formRegistrationPrivacy);?> </p>
									</div>

								</div>

								<div class = "col-md-6" id = "col_social_form"> 

									<div class="col-sm-12 col-form-comming-users">

										<form method="post" id="formsubmit" action="" class="formCommingUsers">

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationName); ?>*</label>
												<?php echo $this->Form->input('name', ['label' => false,'class'=>'form-control']); ?>
												<span id="form_error1" class="help-block help-block-error"></span>
											</div>

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationEmail); ?>*</label>
												<?php echo $this->Form->input('email', ['label' => false,'class'=>'form-control', 'type'=>'email']); ?>
												<span id="form_error1" class="help-block help-block-error"></span>
											</div>

											<div class="form-group">

												<label class="control-label"><?php echo __(formRegistrationMobile); ?>*</label>
												<div class="row">

													<div class="col-sm-3" style="clear: both;">
														<img id="countryflag" src="../img/flags/br.png" width="50"/>
													</div>

													<div class="col-sm-9">
														<select id="countries_dropdown" class="form-control" onChange="countyChanged()"> 

															<?php foreach($countries as $country){ 
															if(file_exists($_SERVER["DOCUMENT_ROOT"].'/app/webroot/img/flags/'.strtolower($country->iso2).'.png')){ ?>
																<option value="<?php echo strtolower($country->iso2) ?>" data-contry-id="<?php echo $country->id; ?>" data-phone="<?php echo $country->phonecode; ?>" data-flag="<?php echo '/app/webroot/img/flags/'.strtolower($country->iso2).'.png';  ?>"><?php echo $country->name; ?></option>
															<?php }}?>

														</select>
													</div>

													<span id="form_error71" class="help-block help-block-error"></span>

												</div>

												<br/>

												<div class="row">

													<div class="col-sm-3" style="clear: both;">
													<input type="text" id="countrycode" class="form-control" value="+55" readonly />
													</div>

													<div class="col-sm-9">
													<?php echo $this->Form->input('phone', ['label' => false,'class'=>'form-control']); ?>
													</div>

													<span id="form_error7" class="help-block help-block-error"></span>
												
												</div>

											</div>

										</form>
									</div>

									<div class="col-sm-6 fb fb-login"> <a id="get_values_facebook" href="javascript:void(0);" ><img src="<?php echo $url; ?>"></a> </div>
									<div class="col-sm-6 you_tube"> <a  href="javascript:void(0);" id="get_values_gmail"> <img src="<?php echo $yurl;?>"></a> </div> 

								</div>

								<div class="row fb_public fb_login_box" style="display:none">
									<div class="col-sm-12 fb_border">

										<div class="col-sm-12">
											<p class="heading"><?php echo __(social_select2);?></p>
										</div>

										<div class="col-sm-12">
											<img src="<?php echo $gifImage; ?>" >
										</div>

										<div class="col-sm-12">
											<button type="button" class="btn back_btn" style="float:left;margin-left:10px;"  onclick="$('.fb_login_box').hide();$('#col_social_text, #col_social_form, .flag_row, .img_row').show();">  <i class=" fa fa-angle-left"></i> <?php echo __(back);?>  </button>
											<button type="button" class="btn next_btn"  id="get_values"><?php echo __(next);?>  <i class=" fa fa-angle-right"></i></button>
										</div>

									</div>
								</div>

								<div class="row logo_row tutorial-step" data-tutorial-name = "<?= __(sharingTitle) ?>" data-tutorial-text = "<?= __(sharingText) ?>">

								<div class="col-sm-12"> 

									<hr>
									<h5 id = "share_text"> <b> <?php echo __(ShareThisInvitationToYourFriends); ?> </b> </h5>
									<div class="button-preview-copy ">
										<div class="sharethis-inline-share-buttons" 
											data-title="<?php echo __(sharethis_title);?>" 
											data-url="<?php  echo HTTP_ROOT; ?>commingusers/view-form?id=<?php echo $FormListingData['form_id']; ?>" 
											data-description="<?php echo __(sharethis_description);?>" data-image="<?php echo HTTP_ROOT.'img/uploads/'.(@$siteinfo->site_logo != ''?@$siteinfo->site_logo:'logo.png'); ?>">
										</div>
									</div>

								</div>

							</div>

						</div>

						<div id = "footer_card">

							<div class = "row" id = "footer_wrapper">

								<div class = "col-md-6">
								<p><a href="https://multiplierapp.com.br/"> <img class = "img-zoom" id = "footer_logo" src="https://multiplierapp.com.br/app/img/logo_big.jpg"> </a> </p>
								</div>

								<div class="col-md-5" id = "footer_login_wrapper">
									<a id="create_account_link" class = "btn btn-multiplier" href = "https://multiplierapp.com.br/politicas-de-uso/"> Criar Conta </a>
									<a id="login_link" class = "btn btn-multiplier btn-green" href = "https://multiplierapp.com.br/app/users/login"> Conecte-se </a>
								</div>

							</div>

							<div class="row advertising_row">

								<div class="col-md-6"> 
									<iframe width="100%" height="315" src="https://www.youtube.com/embed/Z4j3bwCHKqo" frameborder="0" allowfullscreen></iframe>
								</div>

								<div class = "col-md-6">
									<iframe width="100%" height="315" id="youtube_video" src="https://www.youtube.com/embed/F13Gr68_yZo?feature=oembed&amp;start&amp;end&amp;wmode=opaque&amp;loop=0&amp;controls=1&amp;mute=0&amp;rel=0&amp;modestbranding=0"> </iframe>
								</div>

							</div>

							<div class="row knowMore_row">

								<div class="col-md-6">
									<p> <?php echo __(KnowMoreAboutUsAt); ?> <a href="http://www.multiplierapp.live" target="_blank">multiplierapp.live</a></p>
								</div>

								<div class = "col-md-5" id = "icons">

									<a target="_blank" href = "https://www.instagram.com/multiplierapp/"> <i class="fab fa-instagram img-zoom"></i> </a>
									<a target="_blank" href = "https://www.facebook.com/multiplierapp/"> <i class="fab fa-facebook-f img-zoom"></i> </a>
									<a target="_blank" href = "https://www.youtube.com/c/MultiplierApp"> <i class="fab fa-youtube img-zoom"></i> </a>
									<a target="_blank" href = "https://twitter.com/MultiplierApp"> <i class="fab fa-twitter img-zoom"></i> </a>
									<a target="_blank" href = "https://www.linkedin.com/company/multiplierapp/"> <i class="fab fa-linkedin-in img-zoom"></i> </a>

								</div>

							</div>

							<div class = "row">
								
								<div class = "col-md-12" id = "app_stores_wrapper">

									<div id = "app_stores">
										<a target="_blank" href = "https://play.google.com/store/apps/details?id=com.br.multiplierapp"> <img class = "img-zoom footer-tag-img" src = "http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2019/11/Multiplierapp-Baixe-no-google-play.png"> </a>
										<a target="_blank" href = "https://apps.apple.com/app/multiplierapp/id1301426934?ls=1"> <img class = "img-zoom footer-tag-img" src = "http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2019/11/Multiplierapp-Baixe-no-apple-store.png"> </a>
									</div>
								</div>

							</div>

							<div class="row">

								<div class = "col-md-12">
									<p id = "text_rights">@2020 MultiplierApp – Todos os direitos reservados / All rights reserved / Todos los derechos reservados.</p>
								</div>

							</div>
						</div>

					</div>

				</div>
			</div>

		</div>
		<!-- END CONTENT BODY -->
	</div>
	
	<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
	</div>

	<div class = "tutorial-overlay">
	</div>

	<div class = "hide" id = "images_modal">

		<div class = "row" id = "images_wrapper">

			<i class="fas fa-times img-zoom" id = "close_modal_images"></i>

			<div class = "col-sm-4" id = "images_preview">

				<h4 id = "gallery_name"> <?= __(gallery) ?> </h4>
				<hr>

				<div class="portlet-body">
					<div class="panel-group accordion" id="accordion">

						<!-- Default Images -->
						<div class="panel panel-default panel-image">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "avaliable_images_accordion" data-toggle="collapse" data-parent="#accordion" href="#avaliable_images" aria-expanded="false"> <?= __(avaliableImages) ?> </a>
								</h4>
							</div>
							<div id="avaliable_images" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
		
									<?php foreach($elecStaticImageData as $avaliable_image) { ?>
										<div class = "select-image-item background">
											<img class = "img-zoom" data-id = "<?= $avaliable_image->id ?>" src = "<?= $staticimagepath.$avaliable_image->img_name ?>">
										</div>
									<?php } ?>

								</div>
							</div>
						</div>

						<!-- Default Videos -->
						<div class="panel panel-default panel-video">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "avaliable_videos_accordion" data-toggle="collapse" data-parent="#accordion" href="#avaliable_videos" aria-expanded="false"> <?= __(avaliableVideos) ?></a>
								</h4>
							</div>
							<div id="avaliable_videos" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
		
									<?php foreach($elecStaticVideoData as $avaliable_video) { ?>
										<div class = "select-image-item videos">
											<video class = "img-zoom">
												<source src="<?= $staticvideopath.$avaliable_video->video_name ?>" data-id="<?= $avaliable_video->id ?>" data-val="<?= $staticvideopath.$avaliable_video->video_name ?>" type="video/mp4">
											</video>
										</div>
									<?php } ?>
									
								</div>
							</div>
						</div>

						<!-- User Images -->
						<div class="panel panel-default panel-image">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "my_images_accordion" data-toggle="collapse" data-parent="#accordion" href="#my_images" aria-expanded="false"> <?= __(myImages) ?></a>
								</h4>
							</div>
							<div id="my_images" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
								
									<div class = "select-image-item add-file-wrapper">
										<div>
											<i class="fas fa-plus-circle add-file" id = "add_image"></i>
										</div>
									</div>

									<?php foreach($elecDataImage as $user_image) { ?>
										<div class = "select-image-item background">
											<i class="fas fa-trash-alt img-zoom delete-image-icon"></i>
											<img class = "img-zoom" data-id = "<?= $user_image->id ?>" src = "<?= $formingpath.$user_image->videoName ?>">
										</div>
									<?php } ?>

								</div>
							</div>
						</div>

						<!-- User Logos -->
						<div class="panel panel-default panel-image">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "my_logos_accordion" data-toggle="collapse" data-parent="#accordion" href="#my_logos" aria-expanded="false"> <?= __(myLogos) ?></a>
								</h4>
							</div>
							<div id="my_logos" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
								
									<div class = "select-image-item add-file-wrapper">
										<div>
											<i class="fas fa-plus-circle add-file" id = "add_logo"></i>
										</div>
									</div>

									<?php foreach($elecDataLogo as $user_image) { ?>
										<div class = "select-image-item logos">
											<i class="fas fa-trash-alt img-zoom delete-image-icon"></i>
											<img class = "img-zoom" data-id = "<?= $user_image->id ?>" src = "<?= $formingpath.$user_image->videoName ?>">
										</div>
									<?php } ?>

								</div>
							</div>
						</div>

						<!-- User Videos -->
						<div class="panel panel-default panel-video">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "my_videos_accordion" data-toggle="collapse" data-parent="#accordion" href="#my_videos" aria-expanded="false"> <?= __(myVideos) ?></a>
								</h4>
							</div>
							<div id="my_videos" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">
								
									<div class = "select-image-item add-file-wrapper">
										<div>
											<i class="fas fa-plus-circle add-file" id = "add_video"></i>
										</div>
									</div>

									<?php foreach($elecData as $user_video) { ?>
										<div class = "select-image-item videos">
											<i class="fas fa-trash-alt img-zoom delete-image-icon"></i>
											<video class = "img-zoom">
												<source src="<?= $dynamicvideopath.$user_video->videoName_thumb ?>" data-id="<?= $user_video->id ?>" data-val="<?= $dynamicvideopath.$user_video->videoName_360 ?>" data-val2="<?= $dynamicvideopath.$user_video->videoName_720 ?>" data-val3="<?= $dynamicvideopath.$user_video->videoName ?>" type="video/mp4">
											</video>
										</div>
									<?php } ?>

								</div>
							</div>
						</div>										

						<!-- Solid Color  -->
						<div class="panel panel-default panel-image hide">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled collapsed" id = "colors_accordion" data-toggle="collapse" data-parent="#accordion" href="#colors" aria-expanded="false"> <?= __(colors) ?> </a>
								</h4>
							</div>
							<div id="colors" class="panel-collapse collapse" aria-expanded="false">
								<div class="panel-body">

									<div class = "select-image-item" id = "select_colorpicker">

										<div class="input-group color colorpicker-default" id = "colorpicker" data-color="#000000">                							 
											<span class="input-group-btn">
												<button class="btn default" type="button">
													<i style="background-color: rgb(114, 29, 166);"></i>&nbsp;
												</button>
											</span>
										</div>

										<div class="input text" id = "solid_color_input_wrapper">
											<i class="fas fa-plus" id = "add_solid_color"></i>
											<input type="text" name="theme_color" class="form-control col-md-7 col-xs-12" id="solid_color" value="#000000">
										</div>    

									</div>

								</div>
							</div>
						</div>

					</div>
				</div>

			</div>

			<div class = "col-sm-8" id = "image_show_wrapper">

				<h4 id = "chosen_image"> <?= __(chosenImage) ?> </h4>

				<hr>

				<div id = "image_holder_wrapper">

					<img id = "image_holder" name = "" src ="<?php echo $staticimagepath."image1.jpg"; ?>">

					<img id = "logo_holder" name = "" src = "">

					<video class = "hide" id = "preview_video_holder" controls>
						<source id = "video_holder" src="<?php echo $staticvideopath."video1.mp4"; ?>" type="video/mp4">
					</video>

				</div>

			</div>

		</div>

		<div id = "images_controls">

			<form enctype="multipart/form-data" method="post">
					
				<input class = "hide" value="<?php if(!empty($formId)){echo $formId;} ?>" name="frm_id" id="frm_id">
						
				<?= $this->Form->input('addvideogallary',[
						'type' => 'file',
						'label' =>  false,
						'name' =>'addvideogallary',
						'class'=>'addvideogallary_video_input btn btn-multiplier btn-green hide']);
				?>
					
			</form>

			<a class = "btn btn-multiplier btn-green" id = "confirm_choice"> 
				<span id = "confirm_choice_text"><?= __(confirmImageChoice) ?></span> 
				<span class = "hide" id = "uploading_file_text"> Uploading </span>
				<div class="lds-ellipsis hide"><div></div><div></div><div></div><div></div></div>
			</a>

			<div id = "right_images_controls">
				<a class = "btn btn-multiplier vanilla-rotate hide" data-deg="-90"><i class="fa fa-rotate-right"></i></a>
				<a class = "btn btn-multiplier vanilla-rotate hide" data-deg="90"><i class="fa fa-rotate-left"></i></a>
				<!-- <a class = "btn btn-multiplier" id = "crop_image"> <?= __(resizeImage) ?> </a> -->
				<a class = "btn btn-multiplier btn-green hide" id = "crop_image_confirm"> <?= __(save) ?> </a>
				<a class = "btn btn-multiplier hide" id = "crop_image_cancel"> <?= __(cancel) ?> </a>
			</div>

		</div>
		
	</div>

	<div id = "form_functions">
		
		<div class = "img-zoom tooltip-tippy tutorial-step" 
			data-tutorial-name = "<?= __(resourcesTitle) ?>" 
			data-tutorial-text = "<?= __(resourcesText) ?>"
			id = "preview_button" data-tippy-content = "<?= __(previewInvitation) ?>"
		>
			<i class = "far fa-eye"></i>
		</div>

		<div class = "img-zoom tooltip-tippy" id = "gallery_button" data-tippy-content = "<?= __(openGallery) ?>">
			<i class = "far fa-images"></i>
		</div>

		<?php if($formId) { ?>

			<div class = "img-zoom tooltip-tippy" id = "copy_link_button" data-tippy-content = "<?= __(copyInvitationLink) ?>">
				<i class="fas fa-link" ></i>
				<span class = "hide" id = "invitation_link"><?php echo HTTP_ROOT; ?>commingusers/view-form?id=<?php echo $formId; ?></span>
			</div>

			<div class = "img-zoom tooltip-tippy" id = "view_invite" data-tippy-content = "<?= __(viewInvite) ?>">
				<a href = "<?php echo HTTP_ROOT; ?>commingusers/view-form?id=<?php echo $formId; ?>" target = _blank > <i class = "fas fa-external-link-alt"></i> </a>
			</div>

		<?php } ?>

		<div class = "img-zoom tooltip-tippy" id = "tutorial_button" data-tippy-content = "<?= __(showTutorialAgain) ?>">
			<i class = "fas fa-info"></i>
		</div>

		<div class = "img-zoom tooltip-tippy" id = "submit_button" data-tippy-content = "<?= __(saveInvite) ?>">
			<i class = "fas fa-save"></i>
		</div>
		
	</div>

	<div class = "hide" id = "share_modal">
	
		<i class="fas fa-times img-zoom" id="close_share_modal"></i>

		<h2>EU QUERO MULTIPLICAR</h2>
		<p>Escolha qual canal voce deseja publicar</p>

		<div class = "row" id = "share_card_row">

			<div class = "share-card-wrapper">

				<div class = "share-card" id = "personal_profile">

					<svg version="1.1" class = "share-card-icon" id = "personal_profile_icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 511.997 511.997" style="enable-background:new 0 0 511.997 511.997;" xml:space="preserve">
						<g>
							<g>
								<path d="M157.897,185.906c-3.217-2.609-7.939-2.117-10.549,1.101c-3.03,3.736-7.047,5.793-11.313,5.793
									c-4.266,0-8.283-2.058-11.313-5.793c-2.609-3.217-7.333-3.709-10.549-1.101c-3.217,2.609-3.71,7.332-1.101,10.549
									c5.847,7.21,14.217,11.345,22.963,11.345c8.746,0,17.115-4.135,22.963-11.345C161.607,193.238,161.114,188.515,157.897,185.906z"
									/>
							</g>
						</g>
						<g>
							<g>
								<path d="M101.759,141.386c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
									C109.259,144.744,105.901,141.386,101.759,141.386z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M170.311,141.386c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.357,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
									C177.811,144.744,174.453,141.386,170.311,141.386z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M406.397,228.75c-3.217-2.609-7.94-2.117-10.549,1.101c-3.03,3.735-7.048,5.793-11.313,5.793
									c-4.266,0-8.283-2.058-11.313-5.793c-2.608-3.217-7.332-3.71-10.549-1.101c-3.217,2.609-3.709,7.332-1.1,10.549
									c5.846,7.211,14.216,11.345,22.962,11.345c8.746,0,17.115-4.135,22.962-11.345C410.106,236.082,409.614,231.359,406.397,228.75z"
									/>
							</g>
						</g>
						<g>
							<g>
								<path d="M350.259,184.231c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
									C357.759,187.589,354.401,184.231,350.259,184.231z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M418.811,184.231c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.357,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
									C426.311,187.589,422.953,184.231,418.811,184.231z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M491.126,332.545l-58.757-23.503c-0.318-0.127-0.612-0.289-0.91-0.445c40.583-9.234,59.803-24.676,60.656-25.375
									c2.029-1.663,3.042-4.265,2.671-6.862c-0.085-0.594-8.494-60.135-8.494-118.904c0-56.11-45.649-101.759-101.759-101.759
									c-56.11,0-101.758,45.648-101.758,101.759c0,15.374-0.698,34.178-1.834,51.979l-16.373,8.187l-48.06-24.029
									c-0.138-0.069-0.283-0.119-0.424-0.179c2.286-6.663,3.781-13.689,4.33-20.98c10.053-3.106,17.378-12.487,17.378-23.547
									c0-7.449-3.328-14.131-8.569-18.653v-49.9c0-13.785-6.965-26.683-18.387-34.433c-3.87-14.38-16.79-24.481-31.958-24.481h-68.552
									c-37.21,0-67.483,30.273-67.483,67.483v41.33c-5.241,4.521-8.569,11.204-8.569,18.653c0,11.06,7.325,20.441,17.378,23.547
									c1.894,25.179,14.87,47.302,34.036,61.54v20.73L23.664,273.31C9.51,277.557,0,290.338,0,305.116v83.701c0,4.142,3.358,7.5,7.5,7.5
									h1.069v86.759c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-86.759h27.845v86.759c0,4.142,3.358,7.5,7.5,7.5
									c4.142,0,7.5-3.358,7.5-7.5v-129.32c0-6.578-1.929-12.948-5.577-18.42l-12.82-19.23c-2.298-3.447-6.955-4.377-10.4-2.081
									c-3.447,2.297-4.378,6.954-2.08,10.401l12.82,19.231c2,3,3.058,6.492,3.058,10.099v27.561H16.069H15v-76.201
									c0-8.103,5.214-15.11,12.976-17.439l25.747-7.724c8.966,37.542,43.017,64.95,82.312,64.95c14.117,0,27.827-3.481,40.104-10.105
									c4.487,6.121,11.724,10.105,19.878,10.105h1.069v1.069c0,25.143,13.832,47.103,34.276,58.712v12.233
									c0,0.46-0.294,0.868-0.731,1.014l-27.977,9.326c-13.581,4.527-22.706,17.187-22.706,31.503v24.316c0,4.142,3.358,7.5,7.5,7.5
									c4.142,0,7.5-3.358,7.5-7.5V458.76c0-7.849,5.003-14.791,12.449-17.272l8.227-2.743c10.884,16.232,29.189,26.125,48.944,26.125
									c19.776,0,38.064-9.887,48.946-26.124l8.225,2.742c7.446,2.482,12.449,9.423,12.449,17.272v24.316c0,4.142,3.358,7.5,7.5,7.5
									c4.142,0,7.5-3.358,7.5-7.5V458.76c0-14.316-9.125-26.976-22.706-31.502l-27.977-9.326c-0.437-0.146-0.731-0.553-0.731-1.014
									v-12.233c15.307-8.692,26.893-23.188,31.753-40.481c14.173,14.83,33.968,23.546,55.005,23.546
									c33.212,0,62.353-21.469,72.391-52.728l28.629,11.451c6.953,2.781,11.445,9.417,11.445,16.904v35.079h-1.069h-26.776v-9.638
									c0-4.142-3.358-7.5-7.5-7.5c-4.142,0-7.5,3.358-7.5,7.5v94.259c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-69.621
									h19.276v69.621c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-69.621h1.069c1.989,0,3.896-0.79,5.303-2.196
									c1.407-1.407,2.197-3.314,2.197-5.304v-42.579C512,349.719,503.807,337.617,491.126,332.545z M297.776,157.455
									c0-47.839,38.92-86.759,86.759-86.759c47.839,0,86.758,38.92,86.758,86.759c0,50.363,6.065,101.274,8.036,116.479
									c-6.721,4.397-23.929,14.081-53.018,20.445v-11.935c3.984-2.258,7.798-4.835,11.384-7.733
									c14.758-11.929,25.187-28.636,29.363-47.042c1.392-6.133,2.098-12.458,2.098-18.799c0-42.391-30.657-48.477-60.305-54.363
									c-18.865-3.745-38.372-7.618-53.906-20.045c-2.251-1.801-5.335-2.152-7.934-0.903c-2.599,1.249-4.251,3.876-4.251,6.76
									c0,0.199-0.327,19.933-28.595,27c-4.019,1.005-6.462,5.077-5.457,9.096c1.004,4.019,5.077,6.46,9.095,5.457
									c23.66-5.915,33.312-19.188,37.248-29.16c16.264,9.638,34.483,13.254,50.879,16.509c31.689,6.291,48.226,10.912,48.226,39.65
									c0,5.226-0.581,10.434-1.726,15.48c-3.435,15.134-12.016,28.876-24.165,38.696c-12.5,10.104-27.622,15.444-43.73,15.444
									c-14.684,0-28.659-4.504-40.519-13.015c-4.345-11.859-11.324-22.709-20.533-31.649v-33.525c0-2.6-1.346-5.014-3.557-6.38
									c-2.211-1.366-4.971-1.491-7.297-0.328l-16.193,8.096C297.324,186.071,297.776,171.221,297.776,157.455z M308.483,212.435v27.143
									l-27.144-13.571L308.483,212.435z M220.655,212.435l27.143,13.572l-27.143,13.571V212.435z M179.948,294.558v7.123
									c-5.37,4.637-8.569,11.423-8.569,18.583c-10.682,6.311-22.817,9.639-35.345,9.639c-32.624,0-60.849-22.958-67.91-54.27
									l18.529-5.559c4.571,23.088,24.972,40.553,49.382,40.553c19.383,0,36.231-11.017,44.641-27.112
									C180.195,287.157,179.948,290.845,179.948,294.558z M100.69,260.27v-17.373c10.762,4.97,22.734,7.747,35.345,7.747
									c12.61,0,24.583-2.778,35.345-7.746v17.383c0,19.489-15.855,35.345-35.345,35.345c-19.457,0-35.29-15.804-35.343-35.249
									C100.692,260.341,100.69,260.305,100.69,260.27z M136.035,235.644c-38.39,0-69.621-31.231-69.621-69.62c0-4.142-3.358-7.5-7.5-7.5
									c-5.314,0-9.638-4.324-9.638-9.638c0-5.314,4.323-9.638,9.638-9.638h17.138c3.228,0,6.094-2.065,7.115-5.128l6.884-20.652
									c17.235-0.389,70.664-2.812,100.751-17.856c3.705-1.853,5.207-6.357,3.354-10.062c-1.852-3.705-6.356-5.207-10.062-3.354
									c-32.281,16.14-98.807,16.346-99.475,16.346c-3.228,0.001-6.094,2.066-7.114,5.129l-6.859,20.578H58.914
									c-0.358,0-0.714,0.012-1.069,0.027V88.903c0-28.939,23.544-52.483,52.483-52.483h68.552c8.956,0,16.48,6.455,17.893,15.347
									c0.359,2.26,1.73,4.232,3.723,5.356c8.468,4.776,13.729,13.669,13.729,23.21v43.941c-0.355-0.016-0.71-0.027-1.069-0.027h-8.569
									c-4.142,0-7.5,3.358-7.5,7.5c0,4.142,3.358,7.5,7.5,7.5h8.569c5.314,0,9.638,4.323,9.638,9.638s-4.323,9.638-9.638,9.638
									c-4.142,0-7.5,3.358-7.5,7.5C205.656,204.412,174.425,235.644,136.035,235.644z M186.379,260.283v-26.31
									c7.461-5.543,13.982-12.276,19.276-19.928v19.781c-8.399,8.154-14.946,17.895-19.329,28.543
									C186.355,261.676,186.379,260.982,186.379,260.283z M298.548,433.756c-8.257,10.087-20.661,16.113-33.979,16.113
									c-13.302,0-25.714-6.03-33.974-16.114l4.78-1.593c6.572-2.19,10.988-8.317,10.988-15.244v-5.978
									c5.796,1.627,11.898,2.516,18.207,2.516c6.309,0,12.411-0.889,18.207-2.516v5.978c-0.001,6.926,4.414,13.053,10.987,15.243
									L298.548,433.756z M324.553,329.903c-4.142,0-7.5,3.358-7.5,7.5v7.457c0,0.065-0.001,0.13,0,0.195v0.917
									c-0.001,28.939-23.545,52.483-52.484,52.483c-28.939,0-52.482-23.544-52.482-52.483v-8.569c0-4.142-3.358-7.5-7.5-7.5h-8.569
									c-5.314,0-9.638-4.323-9.638-9.638c0-3.42,1.81-6.515,4.841-8.279c0.677-0.394,1.266-0.893,1.776-1.457
									c18.54-0.537,78.639-4.026,118.138-26.597c3.596-2.055,4.846-6.636,2.791-10.233c-2.055-3.597-6.638-4.846-10.233-2.791
									c-34.602,19.772-88.974,23.731-108.744,24.522v-0.872c0-13.949,4.224-27.458,11.878-38.847c0.599,0.949,1.4,1.773,2.386,2.382
									c1.205,0.744,2.572,1.12,3.943,1.12c1.146,0,2.295-0.263,3.354-0.792l48.06-24.03l48.06,24.03
									c1.059,0.529,2.208,0.792,3.354,0.792c1.372,0,2.739-0.376,3.943-1.12c0.985-0.609,1.786-1.434,2.385-2.382
									c7.654,11.389,11.878,24.898,11.878,38.847v10.946c0,2.67,1.419,5.139,3.728,6.482c3.032,1.765,4.842,4.859,4.842,8.279
									c0,5.314-4.324,9.638-9.638,9.638H324.553z M384.535,372.748c-20.764,0-40.028-10.571-51.242-27.849
									c13.506-0.093,24.466-11.106,24.466-24.633c0-7.16-3.199-13.947-8.569-18.584v-7.123c0-3.019-0.163-6.021-0.482-8.995
									c11.121,5.214,23.263,7.925,35.827,7.925c9.145,0,18.189-1.515,26.776-4.379v10.983c0,10.133,6.079,19.113,15.487,22.876
									l16.173,6.469C435.22,355.066,411.55,372.748,384.535,372.748z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M286.432,357.285c-3.217-2.609-7.94-2.117-10.549,1.101c-3.031,3.736-7.048,5.793-11.313,5.793
									c-4.265,0-8.283-2.057-11.313-5.793c-2.609-3.217-7.333-3.71-10.549-1.101c-3.217,2.609-3.71,7.332-1.101,10.549
									c5.848,7.209,14.217,11.345,22.963,11.345c8.746,0,17.115-4.135,22.963-11.344C290.141,364.618,289.648,359.894,286.432,357.285z"
									/>
							</g>
						</g>
						<g>
							<g>
								<circle cx="239" cy="328.868" r="7.5"/>
							</g>
						</g>
						<g>
							<g>
								<circle cx="290" cy="328.868" r="7.5"/>
							</g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
					</svg>
					<p> <b>Perfil Pessoal</b> </p>

				</div>

			</div>

			<div class = "share-card-wrapper">

				<div class = "share-card" id = "group">

					<svg version="1.1" class = "share-card-icon" id = "group_icon" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
						<g>
							<g>
								<path d="M344.214,276.755l-43.582-19.37c-3.079-1.368-5.067-4.429-5.067-7.797V237.62c14.64-11.665,24.05-29.628,24.05-49.758
									v-16.847c4.823-2.665,8.016-7.815,8.016-13.583v-1.635c0-35.076-28.537-63.613-63.613-63.613h-16.032
									c-35.076,0-63.613,28.537-63.613,63.613v14.835c0,5.07,2.182,9.663,5.985,12.602c0.645,0.498,1.324,0.936,2.031,1.315v3.312
									c0,20.13,9.409,38.093,24.048,49.757v11.969c0,3.368-1.989,6.429-5.067,7.797l-43.582,19.37
									c-14.273,6.343-23.496,20.535-23.496,36.154v99.404c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-99.403
									c0-0.869,0.062-1.728,0.152-2.58l20.082,20.082c3.125,3.126,4.847,7.281,4.847,11.702v70.2c0,4.142,3.358,7.5,7.5,7.5
									c4.142,0,7.5-3.358,7.5-7.5v-70.2c0-8.427-3.281-16.349-9.24-22.308l-23.941-23.942c2.157-2.238,4.745-4.093,7.689-5.402
									l42.847-19.043l31.775,31.775v109.119c0,4.142,3.358,7.5,7.5,7.5s7.5-3.358,7.5-7.5V303.194l31.775-31.775l42.846,19.042
									c2.944,1.309,5.532,3.164,7.689,5.403l-23.941,23.941c-5.959,5.959-9.24,13.881-9.24,22.308v70.2c0,4.142,3.358,7.5,7.5,7.5
									c4.142,0,7.5-3.358,7.5-7.5v-70.2c0-4.42,1.721-8.576,4.847-11.702l20.081-20.081c0.09,0.852,0.152,1.709,0.152,2.578v99.404
									c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-99.403C367.709,297.29,358.487,283.098,344.214,276.755z M256,289.481
									l-27.881-27.882c2.12-3.567,3.317-7.694,3.317-12.011v-3.052c7.563,3.178,15.862,4.939,24.565,4.939s17.001-1.76,24.563-4.938
									v3.051c0,4.318,1.197,8.444,3.318,12.012L256,289.481z M256.001,236.474c-26.805,0.001-48.613-21.807-48.613-48.612v-3.373
									c4.68-1.855,9.248-4.807,13.621-8.842c3.043-2.809,3.234-7.554,0.424-10.598c-2.809-3.043-7.553-3.234-10.598-0.425
									c-3.751,3.463-7.499,5.7-11.137,6.65c-0.088,0.023-0.153,0.033-0.199,0.037c-0.054-0.105-0.127-0.325-0.127-0.678v-14.835
									c0-26.805,21.808-48.613,48.613-48.613h16.032c26.806,0,48.613,21.808,48.613,48.613v1.635c0,0.243-0.146,0.454-0.338,0.492
									c-44.911,8.87-67.437-7.847-75.037-15.447c-2.929-2.929-7.678-2.929-10.606,0c-2.929,2.929-2.929,7.678,0,10.606
									c23.29,23.29,57.275,23.71,77.965,21.224v13.553C304.614,214.666,282.806,236.474,256.001,236.474z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M135.242,235.961v-16.035c0-19.566-10.168-36.79-25.489-46.702c0.946-3.035,1.44-6.214,1.44-9.411
									c0-17.396-14.152-31.548-31.548-31.548s-31.548,14.153-31.548,31.548c0,3.237,0.49,6.385,1.438,9.413
									c-15.32,9.911-25.487,27.135-25.487,46.7v16.032c0,18.947,9.533,35.701,24.048,45.743v16.493c0,3.253-1.807,6.177-4.716,7.632
									l-25.941,12.97C6.682,324.176,0,334.988,0,347.015v65.299c0,4.142,3.358,7.5,7.5,7.5s7.5-3.358,7.5-7.5v-62.055l12.218,12.218
									c3.125,3.126,4.847,7.281,4.847,11.702v38.135c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-38.135
									c0-8.427-3.282-16.349-9.24-22.308L20.82,334.866c-0.082-0.082-0.17-0.153-0.254-0.23c1.067-0.942,2.264-1.762,3.582-2.422
									l25.941-12.97c1.329-0.665,2.566-1.45,3.719-2.322l18.337,18.338v77.055c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5
									v-77.056l18.338-18.338c1.154,0.873,2.39,1.658,3.719,2.322l7.153,3.577c1.077,0.539,2.221,0.794,3.349,0.794
									c2.751,0,5.4-1.52,6.714-4.147c1.853-3.705,0.351-8.21-3.354-10.062l-7.153-3.577c-2.91-1.455-4.717-4.379-4.717-7.631v-16.493
									C125.709,271.662,135.242,254.907,135.242,235.961z M79.645,147.265c9.125,0,16.548,7.424,16.548,16.548
									c0,1.005-0.09,1.99-0.262,2.955c-5.154-1.582-10.621-2.439-16.286-2.439c-5.657,0-11.117,0.854-16.263,2.432
									c-0.173-0.965-0.285-1.945-0.285-2.948C63.097,154.688,70.52,147.265,79.645,147.265z M79.645,179.33
									c22.385,0,40.597,18.212,40.597,40.597v2.718c-31.225,9.56-49.834-14.193-50.629-15.234c-1.305-1.739-3.3-2.827-5.468-2.981
									c-2.168-0.152-4.298,0.641-5.835,2.178c-7.159,7.159-14.171,10.612-19.235,12.279C39.63,196.981,57.609,179.33,79.645,179.33z
									M79.645,321.545l-17.331-17.331c0.506-1.934,0.783-3.95,0.783-6.019v-9.155c5.229,1.634,10.788,2.515,16.548,2.515
									s11.319-0.881,16.548-2.515v9.155c0,2.069,0.277,4.086,0.783,6.019L79.645,321.545z M79.645,276.555
									c-22.385,0-40.597-18.212-40.597-40.597v-1.554c5.981-1.374,14.849-4.567,24.1-12.007c3.273,3.088,7.951,6.876,13.908,10.125
									c7.046,3.843,16.777,7.398,28.663,7.398c4.517,0,9.35-0.525,14.459-1.731C119.015,259.541,101.281,276.555,79.645,276.555z"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M494.561,302.765l-41.973-20.987c-2.909-1.455-4.716-4.379-4.716-7.631v-10.629c9.825-8.723,16.032-21.435,16.032-35.575
									v-8.537c0.173,0,0.343,0.005,0.516,0.005c4.142,0,7.5-3.358,7.5-7.5v-16.032c0-26.236-21.345-47.581-47.581-47.581h-16.032
									c-26.236,0-47.581,21.345-47.581,47.581v16.032c0,4.142,3.358,7.5,7.5,7.5c0.062,0,0.242-0.002,0.516-0.013v8.545
									c0,14.141,6.208,26.853,16.033,35.576v10.629c0,3.253-1.807,6.177-4.717,7.631c-3.705,1.852-5.207,6.357-3.354,10.062
									c1.314,2.628,3.963,4.148,6.714,4.148c1.127,0,2.271-0.255,3.348-0.793c1.329-0.665,2.566-1.45,3.719-2.322l18.338,18.338v101.103
									c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5V311.21l18.337-18.338c1.154,0.873,2.39,1.658,3.719,2.322l40.615,20.308
									l-12.319,12.319c-5.958,5.959-9.24,13.881-9.24,22.308v62.184c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-62.184
									c0-4.42,1.721-8.576,4.847-11.702l11.669-11.669c0.354,1.357,0.548,2.772,0.548,4.224v81.331c0,4.142,3.358,7.5,7.5,7.5
									c4.142,0,7.5-3.358,7.5-7.5v-81.331C512,318.956,505.318,308.144,494.561,302.765z M375.725,202.868v-6.99h0.001
									c0-17.965,14.616-32.581,32.581-32.581h16.032c17.965,0,32.581,14.616,32.581,32.581v8.331
									c-32.603-1.759-52.278-14.446-52.469-14.571c-1.269-0.846-2.717-1.26-4.158-1.26c-1.936,0-3.856,0.748-5.306,2.197
									C387.811,197.751,380.78,201.205,375.725,202.868z M416.323,297.497l-17.331-17.331c0.506-1.934,0.783-3.95,0.783-6.019v-1.597
									c5.156,1.919,10.731,2.973,16.548,2.973c5.817,0,11.392-1.054,16.548-2.973v1.597c0,2.069,0.276,4.085,0.783,6.019
									L416.323,297.497z M416.323,260.524c-17.965,0-32.581-14.615-32.581-32.581v-12.091h0c5.253-2.099,11.373-5.436,17.601-10.742
									c7.876,4.098,24.354,11.181,47.561,13.521v9.312C448.904,245.908,434.288,260.524,416.323,260.524z"/>
							</g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
					</svg>
					<p> <b>Grupos</b> </p>

				</div>

			</div>

			<div class = "share-card-wrapper">

				<div class = "share-card" id = "page">

					<svg xmlns="http://www.w3.org/2000/svg" class = "share-card-icon" id = "page_icon" viewBox="0 0 512 512" width="512" height="512"><path d="M472,24H40A24.028,24.028,0,0,0,16,48V464a24.028,24.028,0,0,0,24,24H472a24.028,24.028,0,0,0,24-24V48A24.028,24.028,0,0,0,472,24ZM40,40H472a8.009,8.009,0,0,1,8,8V88H32V48A8.009,8.009,0,0,1,40,40ZM472,472H40a8.009,8.009,0,0,1-8-8V104H480V464A8.009,8.009,0,0,1,472,472Z"/><circle cx="120" cy="64" r="8"/><circle cx="88" cy="64" r="8"/><circle cx="56" cy="64" r="8"/><path d="M440,136H72a8,8,0,0,0-8,8V272a8,8,0,0,0,8,8H440a8,8,0,0,0,8-8V144A8,8,0,0,0,440,136ZM308.849,254.465,328,235.313,356.687,264H316ZM296,264H232l32-42.667Zm-84,0H151.365L192,205.95l30.673,43.819Zm220,0H379.313l-45.656-45.657a8,8,0,0,0-11.314,0l-23.192,23.192L270.4,203.2a8,8,0,0,0-12.8,0l-24.825,33.1-34.221-48.888a8,8,0,0,0-13.108,0L131.835,264H80V152H432Z"/><circle cx="304" cy="192" r="8"/><circle cx="288" cy="304" r="8"/><circle cx="256" cy="304" r="8"/><circle cx="224" cy="304" r="8"/><path d="M168,328H72a8,8,0,0,0-8,8v64a8,8,0,0,0,8,8h96a8,8,0,0,0,8-8V336A8,8,0,0,0,168,328Zm-8,16v45.755l-35-28a8,8,0,0,0-10,0l-35,28V344Zm-22.806,48H102.806L120,378.245Z"/><path d="M304,328H208a8,8,0,0,0-8,8v64a8,8,0,0,0,8,8h96a8,8,0,0,0,8-8V336A8,8,0,0,0,304,328Zm-8,16v45.755l-35-28a8,8,0,0,0-10,0l-35,28V344Zm-22.806,48H238.806L256,378.245Z"/><path d="M440,328H344a8,8,0,0,0-8,8v64a8,8,0,0,0,8,8h96a8,8,0,0,0,8-8V336A8,8,0,0,0,440,328Zm-8,16v45.755l-35-28a8,8,0,0,0-10,0l-35,28V344Zm-22.806,48H374.806L392,378.245Z"/><path d="M152,424H88a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16Z"/><path d="M288,424H224a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16Z"/><path d="M424,424H360a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16Z"/>
					</svg>
					<p> <b>Paginas</b> </p>

				</div>			

			</div>

		</div>

		<div class = "row" id = "share_footer_wrapper">

			<div id = "share_footer_img_wrapper">
				<img src="<?php echo $gifImage; ?>">
			</div>

			<div id = "share_footer_text_wrapper">
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
					ut laoreet dolore magna aliquam erat volutpat. Ut
					wisi enim ad minim veniam, quis nostrud exerci
					tation ullamcorper suscipit lobortis nisl ut aliquip 
				</p>
				<img>
				<p> <a href = "#">Política de privacidade</a> e <a href = "#">termos</a> de Multiplierapp</p>
			</div>

		</div>

		<i class = "fab fa-facebook" id = "share_facebook_icon" ></i>

	</div>

</form>

<a class = "hide" target="_blank" href="https://multiplierapp.atlassian.net/servicedesk/customer/portal/1"><img class = "img-zoom hide" src="<?php echo HTTP_ROOT ;?>img/1962073065-Help.png" id="bottom-help-img"></a> 

<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>                 
<script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5b362317c5ed9600115214d3&product=inline-share-buttons"></script>
<script type="text/javascript"> var base_url = '<?php echo HTTP_ROOT; ?>';</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.min.js" integrity="sha256-OeQqhQmzxOCcKP931DUn3SSrXy2hldqf21L920TQ+SM=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.extensions.min.js" integrity="sha256-vpL5m6IilsG6TXFKZ99NU+cBmT122RJ6sqBgq0a9/vQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/jquery.inputmask.min.js" integrity="sha256-x4dnYQ/ZWD3+ubQ1kR3oscEWXKZj/gkbZIhGB//kmmg=" crossorigin="anonymous"></script>
<script src="<?php echo HTTP_ROOT; ?>assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>

<link href="<?php echo HTTP_ROOT;?>assets/global/plugins/croppie/croppie.css" rel="stylesheet" type="text/css" />
<script src="<?php echo HTTP_ROOT;?>assets/global/plugins/croppie/croppie.js" type="text/javascript"></script>

<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo HTTP_ROOT;?>js/jquery_ui_touch_punch.js" type="text/javascript"></script>

<script src="https://cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>

<script>
	/** NEW SHARE JS */

	$("#personal_profile").click(function(){
		window.location.href = base_url + 'commingusers/fblogin?id=' + <?php echo $_GET['id'];?>
	});


</script>

<script>

	/** NEW MAIN TEXT JS */

	var editor_count = 1;
	var column_midia_selection = false;
	var $column_midia;

	var $add_content_element = $(
		`<div>

			<div class = "content-column">

				<div class = "column-functions-wrapper hide">
					<i class="fas fa-trash-alt column-function-icon column-tooltip-tippy delete-column-icon" data-tippy-content = "<?= __(deleteColumn) ?>"></i>
					<i class="far fa-check-circle column-function-icon column-tooltip-tippy save-column-icon hide" data-tippy-content = "<?= __(saveColumn) ?>"></i>
					<i class="fas fa-edit column-function-icon column-tooltip-tippy edit-column-icon hide" data-tippy-content = "<?= __(editColumn) ?>"></i>
					<i class="fas fa-arrows-alt column-function-icon column-tooltip-tippy move-column-icon" data-tippy-content = "<?= __(moveColumn) ?>"></i>
				</div>

				<div class = "add-new-content-wrapper">

					<div class = "add-new add-new-content">

						<div class = "add-new-divider">
							<div class= "icon-wrapper">
								<i class="fas fa-font add-icon add-content-icon column-tooltip-tippy add-new-text" data-tippy-content = "<?= __(addText) ?>"></i>
							</div>
						</div>

						<div class = "add-new-divider">
							<div class= "icon-wrapper">
								<i class="fas fa-photo-video add-icon add-content-icon column-tooltip-tippy add-new-midia" data-tippy-content = "<?= __(addMidia) ?>"></i>
							</div>
						</div>

						<div class = "add-new-divider">
							<div class= "icon-wrapper">
								<i class="fas fa-arrows-alt-v add-icon add-content-icon column-tooltip-tippy add-new-space" data-tippy-content = "<?= __(addSpace) ?>"></i>
							</div>
						</div>

					</div>
					
				</div>

			</div>

		</div>`
	);

	enableResizableColumns();
	tippy(".column-tooltip-tippy");

	//ISSUE
	//PLACEHOLDER BIGGER THAN THE TARGET WHEN USING % FOR WIDTH
	$(".new-content-container").sortable({
		placeholder: "ui-state-highlight",
		handle: ".move-column-icon",
		start: function(event, ui){

			var $target = $(ui.item[0]);
			var $placeholder = $(ui.placeholder[0]);

			$placeholder.width( $target.width() );
			$placeholder.height( $target.height() );
			
		},
	}).disableSelection();

	$(document).on('click', '.add-one-column', function(e){
		addNewContentRow(1);
	});

	$(document).on('click', '.add-two-column', function(e){
		addNewContentRow(2);
	});

	$(document).on('click', '.add-three-column', function(e){
		addNewContentRow(3);
	});

	//COLUMN FUNCTIONS

	$(document).on('mouseenter', '.content-column', function(e){
		$(this).find(".column-functions-wrapper").removeClass("hide");
	});

	$(document).on('mouseleave', '.content-column', function(e){
		$(this).find(".column-functions-wrapper").addClass("hide");
	});

	$(document).on('click', '.add-content-icon', function(e){

		var $this = $(this);
		var $column = $this.closest('.content-column')

		if( $this.hasClass('add-new-text') ){

			cleanColumn($column);

			createCkeditorInstance($column);

			$column.find(".save-column-icon").removeClass("hide");

		}
		else if( $this.hasClass('add-new-midia') ){
			$("#gallery_button").click();

			column_midia_selection = true;
			$column_midia = $column

			$column.find(".edit-column-icon").removeClass("hide");

			hideGalleryModalOptions();
		}
		else if($this.hasClass('add-new-space')){

			cleanColumn($column);

			$column.addClass("chosen-column");
			$column.append( `<div class = "chosen-content space-content"></div>`);
		}

	});

	$(document).on('click', '.edit-column-icon', function(e){
		
		var $this = $(this);
		var $column = $this.closest(".content-column");

		if($column.find(".text-content").length > 0){

			createCkeditorInstance($column, $column.find(".text-content").html() );

			$this.addClass("hide");
			$column.find(".save-column-icon").removeClass("hide");
			
		}
		else if($column.find(".midia-content").length > 0){

			$("#gallery_button").click();

			column_midia_selection = true;
			$column_midia = $column

		}

		$column.find(".chosen-content").remove();
	});

	$(document).on('click', '.delete-column-icon', function(e){

		var $column = $(this).closest(".content-column");

		if( $column.find(".chosen-content").length > 0 ){
			$column.find(".chosen-content").remove();
			$column.height(230);
			$column.append( $add_content_element.find(".content-column").html() )
			$column.removeClass("chosen-column");
		}
		else{
			$column.remove();
		}

	});

	$(document).on('click', '.save-column-icon', function(e){
		
		var $column = $(this).closest(".content-column");

		$column.append( `<div class = "chosen-content text-content">  ${getCkeditorInstanceByName( $column.find(".ckeditor").attr("name") ).getData()} </div>`);
		
		$column.find(".cke").remove();
		$column.find(".ckeditor").remove();

		$column.find(".edit-column-icon").removeClass("hide");
		$column.addClass("chosen-column");
		$(this).addClass("hide");

	});

	//ISSUE
	// RESIZE TO THE LEFT OVERFLOW PARENT
	function enableResizableColumns(){
		$( ".content-column" ).resizable({
			maxWidth: 570,
			// minHeight: 100,
			// minWidth: 100,
			grid: 5,
			resize: function(event, ui){

				var $target = $(ui.element[0]);

				if( $target.find(".cke").length > 0 ){

					$target.find(".cke").width(ui.size.width - 25);
					$target.find(".cke_contents").height( ui.size.height - $target.find(".cke_top").height() - $target.find(".cke_bottom").height() - 60 );

				}

			}
			// handles: 'n, e, s, w',
		});
	}

	function addNewContentRow(columns){

		var content = "";
		var margin = ( $(window).width() > 500 ) ? 1 : 2;
		var column_width = (100/columns) - margin;

		$add_content_element.find(".content-column").width(`${column_width}%`);

		for(var i = 0; i < columns; i++){
			content += $add_content_element.html();
		}

		$(".new-content-container").append( $(content) );

		tippy(".column-tooltip-tippy");
		enableResizableColumns();
	}

	function cleanColumn($column){
		$column.find(".add-new-content-wrapper").remove();
	}

	function createCkeditorInstance($target, content = ""){

		$target.prepend(`<div class = 'ckeditor' name = 'ckeditor-${editor_count}' data-editor = ${editor_count}> ${content} </div>`).height("auto");

		CKEDITOR.replace(`ckeditor-${editor_count}`, {
			customConfig: '<?php echo HTTP_ROOT;?>js/ckeditor_config.js',
			height: 'auto',
		});
			
		editor_count++;
	}

	function getCkeditorInstanceByName(name){

		var instance = null;

		$.each(CKEDITOR.instances, function(index, value){
			if( $(value.container.$).parent().find(".ckeditor").attr("name") == name){
				instance = value;
			}
		});

		return instance;
	}

</script>

<script type = "text/javascript">

	var updatedImage;
	var formId = "<?= ($formId) ? $formId : "" ?>";
	var language = "<?= $selectedLanguage ?>";
	var change = false;
	var croped_result;

	$( document ).ready(function() {
		
		$(".colorpicker-default").colorpicker({format:"hex"}).on('changeColor', function(ev){
			$("#solid_color").val(ev.color.toHex());
		})
		.on('show', function(){
			var left = parseInt($(".colorpicker").css("left").slice(0, -2)) + $(".select-image-item:visible").width() - 20;
			$(".colorpicker").css("left", `${left}px`);
		});

		$("#add_solid_color").click(function(){
			addBase64File( createImageData64FromColor( $("#solid_color").val() ) );
		});

		$("#solid_color").change(function(){
			$(".colorpicker-default").colorpicker('setValue', $(this).val() );
		});

		$(".icon-editable, .column-function-icon, .add-content-icon").click(function(){
			change = true;
		});

		$("a").click(function(e){
			
			if( $(this).attr("href") !== undefined ){

				if( $(this).attr("href").indexOf("http") >= 0 && change){
					
					e.preventDefault();

					$(".container").append(`
						<div class = "tutorial-start-text-box" style = "width: auto;"> 
							<i class="fas fa-info img-zoom" id = "info_message"></i>
							<h3> <?= __(attention) ?> </h3>
							<p> <?= __(saveBeforeExit) ?> </p> 
							<div class = "tutorial-button-wraper">
								<a class = "btn btn-multiplier btn-green" href = "#" onclick = "$('#submit_button').click();"> <?= __(save) ?> </a>
								<a class = "btn btn-multiplier" onclick = "clear_tutorial();" > <?= __(keepEditing) ?> </a>
							</div>
						</div>`
					);

				}

			}
				
		});

		tippy(".tooltip-tippy");

		//Background Image Edition

		$(".select-image-item.background").click(function(){

			$("#preview_video_holder, #logo_holder").addClass("hide");
			$("#image_holder, #crop_image").removeClass("hide");
			$("#image_holder").attr("src", $(this).find('img').attr("src") );
			
			$("#chosen_image_base_64").val( $(this).find('img').attr("data-id") );
			$("#chosen_background_image_base_64").val( $(this).find('img').attr("src") );	

		});

		$(".select-image-item.logos").click(function(){

			$("#preview_video_holder, #image_holder, #crop_image").addClass("hide");
			$("#logo_holder").removeClass("hide");
			$("#logo_holder").attr("src", $(this).find('img').attr('src') );
			
			$("#chosen_header_logo_image_base_64").val( $(this).find('img').attr('src') );	
		});

		$(".select-image-item.videos").click(function(){

			$("#image_holder, #crop_image, #logo_holder").addClass("hide");
			$("#video_holder").attr("src", $(this).find("source").attr("src") );
			$("#preview_video_holder").removeClass("hide").get(0).load();
			
			$("#chosen_video").val( $(this).find("source").attr("data-val") );
			$("#chosen_video_id").val( $(this).find("source").attr("data-id") );
		});

		$(".add-file-wrapper").click(function(){

			if( $(this).find("#add_image").length > 0){
				$('#background_edition_input').click();
			}
			else if( $(this).find("#add_video").length > 0){
				$('#addvideogallary').click();
			}
			else if( $(this).find("#add_logo").length > 0 ){
				$('#header_logo_edition_input').click();
			}
			
		});
		
		$('#background_edition_input').on('change', function () { 
			readFileNew(this, 
			function(e){ 
				addImage(e.target.result);
			});
		});

		$(document).on('click', '.delete-image-icon', function(e){

			delete_element = $(this).parent();

			$(".container").append(`
				<div class = "tutorial-start-text-box" style = 'width: auto;'> 
					<i class="fas fa-info img-zoom" id = "info_message"></i>
					<h3> <?= __(attention) ?> </h3>
					<p> <?= __(deletingFile) ?> </p> 
					<div class = "tutorial-button-wraper">
						<a class = "btn btn-multiplier btn-green" href = "#" onclick = "clear_tutorial(false); deleteItemGallery( delete_element ); $('#images_modal').removeClass('blocked'); "> <?= __(delete) ?> </a>
						<a class = "btn btn-multiplier" onclick = "clear_tutorial(false); $('#images_modal').removeClass('blocked');" > <?= __(go_back) ?> </a>
					</div>
				</div>`
			);

			$("#images_modal").addClass("blocked");

		});

		$("#background_edition, #main_video").click(function(event){

			resizeImagesModal();
	
			var id = $(event.target).prop("id");

			$("#images_modal").removeClass("hide");
			$(".tutorial-overlay").removeClass("hide");
			( $("#avaliable_images:visible").length == 0) ? $('#avaliable_images_accordion').click() : null;

			hideGalleryModalOptions("image");

		});

		$("#close_modal_images").click(function(){
			$("#images_modal").addClass("hide");
			$(".tutorial-overlay").addClass("hide");
			
			column_midia_selection = false;
			$column_midia = null;
		});

		$("#close_share_modal").click(function(){
			$("#share_modal").addClass("hide");
		});

		$("#close_modal_images_video").click(function(){
			$("#images_modal_video").addClass("hide");
			$(".tutorial-overlay").addClass("hide");
		});

		$(document).on("click", ".append-logo" , function() {

			$("#preview_video_holder, #image_holder, #crop_image").addClass("hide");
			$("#logo_holder").removeClass("hide");
			$("#logo_holder").attr("src", $(this).attr('src') );
			
			$("#chosen_header_logo_image_base_64").val( $(this).attr('src') );	
		});		

		$(document).on("click", ".append-img" , function() {

			$("#preview_video_holder, #logo_holder").addClass("hide");
			$("#image_holder, #crop_image").removeClass("hide");
			$("#image_holder").attr("src", $(this).attr("src") );
			
			$("#chosen_image_base_64").val( $(this).attr("data-id") );
			$("#chosen_background_image_base_64").val( $(this).attr("src") );
		});

		$(document).on("click", ".append-video" , function() {
			
			$("#image_holder, #crop_image, #logo_holder").addClass("hide");
			$("#video_holder").attr("src", $(this).find("source").attr("src") );
			$("#preview_video_holder").removeClass("hide").get(0).load();
			
			$("#chosen_video").val( $(this).find("source").attr("data-val") );
			$("#chosen_video_id").val( $(this).find("source").attr("data-id") );

		});

		$(".select-video-tag").click(function(){
			var videopath =  $(this).children("source").attr("src");

			var videoid =  $(this).children("source").attr("data-id");
			var video_360 =  $(this).children("source").attr("data-val");
		
			$("#video_holder_wrapper").html(`
			<video  controls>
				<source id = "video_holder" src="`+video_360+`" type="video/mp4">
			</video>
			`)
			
			$("#chosen_video").val(video_360);
			$("#chosen_video_id").val(videoid);
		});

		$("#confirm_choice").click(function(){

			if(column_midia_selection && $column_midia){
				
				if( $("#image_holder:visible").length > 0  ){
					$column_midia.append(` <div class = "chosen-content midia-content"> <img src = '${$("#chosen_background_image_base_64").val()}'> </div> `);
				}
				else if( $("#preview_video_holder:visible").length > 0 ) {
					$column_midia.append(` <div class = "chosen-content midia-content"> <video controls> <source src = '${$("#chosen_video").val()}'> </video> </div> `);
				}
				else if($("#logo_holder:visible").length > 0){
					$column_midia.append(` <div class = "chosen-content midia-content"> <img src = '${$("#chosen_header_logo_image_base_64").val()}'> </div> `);
				}

				cleanColumn($column_midia);
				$column_midia.height('auto').addClass("chosen-column");
				$("#close_modal_images").click();
			}
			else if( $("#image_holder:visible").length > 0  ){

				var selected_val = $("#chosen_background_image_base_64").val();
				var form_id = "<?php echo $formId; ?>";

				var form_data = new FormData();
				form_data.append("selected_val", selected_val);
				form_data.append('form_id',form_id);
				
				$("#close_modal_images").click();

				var url = window.location.origin + "/app/electronicinvitation/saveSelectedImage";
				$.ajax({				
					url:url,
					method:"POST",
					data: form_data,
					contentType: false,
					cache: false,
					dataType: "text",
					processData: false,
					beforeSend:function(){
						// $(".lds-ellipsis").removeClass("hide");
					},
					success:function(data)
					{
						console.log(data);
						if(data){
							$(".page-content .view_form_page").css( "background-image", `url( ${$("#image_holder").attr("src")} )` );
							updatedImage = $("#image_holder").attr("src");
						}
					},

				});

			}
			else if( $("#logo_holder:visible").length > 0 ){
				$("#close_modal_images").click();
				$("#header_logo").attr("src", $("#chosen_header_logo_image_base_64").val());
			}
			else if( $("#preview_video_holder:visible").length > 0 ){

				var selected_val = $("#chosen_video").val();
				var selected_val_id = $("#chosen_video_id").val();
				var form_id = "<?php echo $formId; ?>";
				//alert(selected_val_id);
				var form_data = new FormData();
				form_data.append("selected_val", selected_val);
				form_data.append("selected_val_id", selected_val_id);
				form_data.append('form_id',form_id);
				
				$("#close_modal_images").click();
				$("#initial-video").children("video").removeClass("hide");
				$("#remove_video").removeClass("hidden");
				$("#initial_video_placeholder").addClass("hide");
				$("#no_video").val(0);

				var url = window.location.origin + "/app/electronicinvitation/saveSelectedVideo";
				$.ajax({
					
					url:url,
					method:"POST",
					data: form_data,
					contentType: false,
					cache: false,
					dataType: "text",
					processData: false,
					beforeSend:function(){
						//$('#loader-show').addClass("loader");
					},
					success:function(data)
					{
						console.log(data);
						//var path = "<?php echo $dynamicvideopath; ?>"+data;
						if(data){
							$("#initial-video video source").attr("src", data );
							$("#initial-video video").get(0).load();
						}
					}
				});

			}

		});

		$("#remove_video").click(function(){

			$(".container").append(`
				<div class = "tutorial-start-text-box" style = 'width: auto;'> 
					<i class="fas fa-info img-zoom" id = "info_message"></i>
					<h3> <?= __(attention) ?> </h3>
					<p> <?= __(removeVideoWarning) ?> </p> 
					<div class = "tutorial-button-wraper">
						<a class = "btn btn-multiplier btn-green" href = "#" onclick = " clear_tutorial(false); removeVideo()"> <?= __(remove) ?> </a>
						<a class = "btn btn-multiplier" onclick = "clear_tutorial(false);" > <?= __(go_back) ?> </a>
					</div>
				</div>`
			);

		});

		//Video
		$("#background_edition_video").click(function(){

			resizeImagesModalVideo();

			$("#images_modal_video").removeClass("hide");
			$(".tutorial-overlay").removeClass("hide");
		});

		//Logo Edition

		$("#header_logo_edition").click(function(){
			// $('#header_logo_edition_input').click();

			resizeImagesModal();

			$("#images_modal").removeClass("hide");
			$(".tutorial-overlay").removeClass("hide");

			( $("#my_logos:visible").length == 0 ) ? $('#my_logos_accordion').click() : null;
			hideGalleryModalOptions("logo");

		});

		$('#header_logo_edition_input').on('change', function () {
			readFile(this, 
			function(e){
				console.log(e);
				// $("#header_logo").attr("src", e.target.result);
				// $("#chosen_header_logo_image_base_64").val(e.target.result);
			}); 
		});

    	//Text Edition

		$("#main_text_edition_button").click(function(){
			
			$("#main_text_edition").removeClass("hide");
			$("#main_text_edition_confirm").removeClass("hide");
			
			$("#main_text").addClass("hide");
			$("#main_text_edition_button").addClass("hide");
		});

		$("#main_text_edition_confirm").click(function(){

			$("#main_text_edition").addClass("hide");
			$("#main_text_edition_confirm").addClass("hide");

			$("#main_text").removeClass("hide");
			$("#main_text_edition_button").removeClass("hide");

			$("#main_text").html( $(".note-editable").html() );

		});

		//Headline Edition

		$("#headline_edition_button").click(function(){

			$("#headline_edition_input").removeClass("hide").val( $("#invitation_headline").text() );
			$("#headline_edition_confirm").removeClass("hide");

			$("#invitation_headline").addClass("hide");
			$("#headline_edition_button").addClass("hide");
		});

		$("#headline_edition_confirm").click(function(){

			$("#headline_edition_input").addClass("hide");
			$("#headline_edition_confirm").addClass("hide");

			$("#invitation_headline").removeClass("hide").text( $("#headline_edition_input").val() );
			$("#headline_edition_button").removeClass("hide");

		});

		// Social Text Edition

		$("#social_title_edition_button").click(function(){

			$("#social_title_edition_input").removeClass("hide").val( $("#social_title").text() );
			$("#social_title_edition_confirm").removeClass("hide");

			$("#social_title").addClass("hide");
			$("#social_title_edition_button").addClass("hide");
		});

		$("#social_title_edition_confirm").click(function(){

			$("#social_title_edition_input").addClass("hide");
			$("#social_title_edition_confirm").addClass("hide");

			$("#social_title").removeClass("hide").text( $("#social_title_edition_input").val() );
			$("#social_title_edition_button").removeClass("hide");

		});

    	//Form Functions

		//Preview
		$("#preview_button").click(function(){

			if( $(".edition-input").not(".hide").length == 0 ){
				$(".page-header").toggleClass("preview");
				$(".page-content").toggleClass("preview");
				$(".page-container").toggleClass("preview");
				$(".icon-editable").not(".next-release").toggleClass("hide");
				$("#preview_button").toggleClass("on");
				$(".hide-content").toggleClass("preview");
				$(".add-new-column").toggleClass("preview");
				$(".new-content-container").toggleClass("preview");
				$(".content-column").toggleClass("preview");
			}
			else{

			$(".container").append(`
				<div class = "tutorial-start-text-box"> 
				<i class="fas fa-times img-zoom" id = "close_error_message"></i>
				<h3> Error </h3>
				<p> Por favor termine todas as edições antes de ver o preview da página! </p> 
				<div class = "tutorial-button-wraper">
					<a class = "btn btn-multiplier btn-green" onclick = "clear_tutorial();" > Okay </a>
				</div>
				</div>`
			);

			}

		});

		//Gallery
		$("#gallery_button").click(function(){
			
			resizeImagesModal();

			$("#images_modal").toggleClass("hide");
			$(".tutorial-overlay").toggleClass("hide");
			( $("#avaliable_images:visible").length == 0) ? $('#avaliable_images_accordion').click() : null;

			hideGalleryModalOptions();

		});

		//Copy Link
		$("#copy_link_button").click(function(){
			var success = copyToClipboard( $("#invitation_link").get(0) );

			if($success){

			$(".container").append(`
				<div class = "tutorial-start-text-box"> 
				<i class="fas fa-times img-zoom" id = "close_error_message"></i>
				<h3> Link copiado com sucesso </h3>
				<div class = "tutorial-button-wraper">
					<a class = "btn btn-multiplier btn-green" onclick = "clear_tutorial();" > Okay </a>
				</div>
				</div>`
			);

			}

		});

		//Tutorial
		$("#tutorial_button").click(function(){
			if( $(".tutorial-start-text-box").length === 0 ){
			start_tutorial();
			}
		});

		//Save
		$("#submit_button").click(function(){

			$(".new-content-container").find("script").remove();
			$(".new-content-container").find("style").remove();
			$(".new-content-container").find(".content-column").not(".chosen-column").remove();
			$(".new-content-container").find(".ui-resizable-handle").remove();

			$("#cause_description_text").remove();
			$("#main_text_html").val( $(".new-content-container").html() );
			$("#edition_form").submit();
		});

		$(".icon-editable").not("#background_edition").hover(
			function(){

				var $parent = $(this).parent();

				if( $parent.hasClass("editable-content") ){
				$parent.addClass("editable-content-highlight");
				}
				else{
				$parent.find(".editable-content").addClass("editable-content-highlight");
				}

			},
			function(){

				var $parent = $(this).parent();

				if( $parent.hasClass("editable-content") ){
				$parent.removeClass("editable-content-highlight");
				}
				else{
				$parent.find(".editable-content").removeClass("editable-content-highlight");
				}
			}
		);

		loadLocalImages();
		loadLocalVideos();
  	});

	function hideGalleryModalOptions(show_option = ""){

		$("#avaliable_images_accordion").parent().parent().parent().hide();
		$("#avaliable_videos_accordion").parent().parent().parent().hide();
		$("#my_images_accordion").parent().parent().parent().hide();
		$("#my_logos_accordion").parent().parent().parent().hide();
		$("#my_videos_accordion").parent().parent().parent().hide();

		switch(show_option){
			case "image":
				$("#avaliable_images_accordion").parent().parent().parent().show();
				$("#my_images_accordion").parent().parent().parent().show();
				break;
			case "video":
				$("#avaliable_videos_accordion").parent().parent().parent().show();
				$("#my_videos_accordion").parent().parent().parent().show();
				break;
			case "logo":
				$("#my_logos_accordion").parent().parent().parent().show();
				break;
			default:
				$("#avaliable_images_accordion").parent().parent().parent().show();
				$("#avaliable_videos_accordion").parent().parent().parent().show();
				$("#my_images_accordion").parent().parent().parent().show();
				$("#my_logos_accordion").parent().parent().parent().show();
				$("#my_videos_accordion").parent().parent().parent().show();
				break;
		}

	}

	function resizeImagesModal(){
		
		var modal_height = $(window).height() - ( $(window).height()/10 ) * 2;

		$("#images_modal").css("height", modal_height);
		$("#images_wrapper").css("height", modal_height - 50);
		$("#images_preview").css("height", modal_height - 50);
		$("#image_holder_wrapper").css("height", modal_height - 100);
	}

	function resizeImagesModalVideo(){
		var modal_height = $(window).height() - ( $(window).height()/10 ) * 2;
		$("#images_modal_video").css("height", modal_height);
		$("#images_wrapper_video").css("height", modal_height - 50);
		$("#images_preview_video").css("height", modal_height - 50);
		$("#video_holder_wrapper").css("height", modal_height - 100);
	}

	function addImage(src, addEvents = true){

		var images = [];

		if(localStorage.getItem("background_images") != null ){
			images = JSON.parse( localStorage.getItem("background_images") );
		}

		// $("#images_preview").append(`
		// 	<div class = "select-image-item">
		// 		<img class = "img-zoom" data-localstorage-id = "${images.length}" src = "${src}">
		// 	</div>
		// `)

		images.push(src)
		localStorage.setItem("background_images", JSON.stringify(images))

		if(addEvents){

			// $(".delete-image-icon").click(function(){
			// 	deleteLocalImage(this);
			// });

			$(".select-image-item img").click(function(){
				//alert();
				$("#image_holder").attr("src", $(this).attr("src") );
				$("#chosen_background_image_base_64").val( $(this).attr("src") );
			});

		}

	}

	function readFileNew(input, callback) {

		if (input.files && input.files[0]) {

			var form_data = new FormData();
			form_data.append("file", input.files[0]);
			form_data.append('form_id',$('#frm_id').val());
			var url = window.location.origin + "/app/electronicinvitation/backgroundImageUpload";
				$.ajax({
				
				url:url,
				method:"POST",
				data: form_data,
				contentType: false,
				cache: false,
				dataType: "text",
				processData: false,
				beforeSend:function(){
					$(".lds-ellipsis, #uploading_file_text").removeClass("hide");
					$("#confirm_choice_text").addClass("hide");
				},
				success:function(data)
				{
					console.log('video data=>',data);
					var formpath = "<?php echo $formingpath; ?>";
					if(data){
						var path = formpath+data;
						$("#my_images .panel-body").append(`
							<div class = "select-image-item background">
								<img class = "img-zoom append-img" data-id = "1" src = "`+path+`">
							</div>
						`)
					}
				
				},
				complete: function(){
					$(".lds-ellipsis, #uploading_file_text").addClass("hide");
					$("#confirm_choice_text").removeClass("hide");
				}
			});
		}
	}

	function addBase64File(base64){

		var form_data = new FormData();
		form_data.append("solid_image_base64", base64);
		form_data.append('form_id', $('#frm_id').val() );
		var url = window.location.origin + "/app/electronicinvitation/solidColorImageUpload";

		$.ajax({
			url:url,
			method:"POST",
			data: form_data,
			contentType: false,
			cache: false,
			dataType: "text",
			processData: false,
			beforeSend:function(){
				//$('#loader-show').addClass("loader");
			},
			success:function(data)
			{
				var formpath = "<?php echo $formingpath; ?>";

				if(data){
					var path = formpath+data;
					$("#my_images .panel-body").append(`
						<div class = "select-image-item">
							<img class = "img-zoom append-img" data-id = "1" src = "`+path+`">
						</div>
					`)
				}
			}
		});

	}

	function deleteItemGallery($element){

		var path_parts = $element.find('img, source').attr('src').split('/');

		var type;
		var path;
		var dont_delete = 0;

		if( path_parts[path_parts.length-2] == "user-images" ){
			
			type = 2;
			path = path_parts[path_parts.length-2] + "/" + path_parts[path_parts.length-1];

			if( 
				// $element.find('img').attr('src') === $("#chosen_background_image_base_64").val() ||
			 $element.find('img').attr('src') === "<?= $formInfo->selected_image ?>"){
				dont_delete = 1;
			}

		}
		else if( path_parts[path_parts.length-2] == "user-videos" ){

			type = 3;

			var video_path_part = $element.find('source').attr("data-val").split('/');

			var video_path_part2 = $element.find('source').attr("data-val2").split('/');

			var video_path_part3 = $element.find('source').attr("data-val3").split('/');

			path = [ path_parts[path_parts.length-2] + "/" + path_parts[path_parts.length-1], video_path_part[video_path_part.length-2] + "/" + video_path_part[video_path_part.length-1], video_path_part2[video_path_part2.length-2] + "/" + video_path_part2[video_path_part2.length-1], video_path_part3[video_path_part3.length-2] + "/" + video_path_part3[video_path_part3.length-1] ]
		
			if( 
				// $element.find('source').attr('data-val') === $("#chosen_video").val() || 
				$element.find('source').attr('data-val').replace("_360", "") === "<?= $formInfo->selected_video ?>"){
				dont_delete = 1;
			}
		
		}
		else{
			dont_delete = 1;
		}

		if(dont_delete){

			$(".container").append(`
				<div class = "tutorial-start-text-box" style = 'width: auto;'> 
					<i class="fas fa-info img-zoom" id = "info_message"></i>
					<h3> <?= __(attention) ?> </h3>
					<p> <?= __(dontDeleteFile) ?> </p> 
					<div class = "tutorial-button-wraper">
						<a class = "btn btn-multiplier btn-green" href = "#" onclick = "clear_tutorial(false);"> <?= __(ok) ?> </a>
					</div>
				</div>`
			);

			return 0;
		}
		//alert(path);
		//alert($element.find('img, source').attr("data-id"));
		//alert(type);
		$element.remove();
		deleteFile(path, $element.find('img, source').attr("data-id"), type );
	}

	function removeVideo(){

		$("#initial-video").children("video").addClass("hide");
		$("#remove_video").addClass("hidden");
		$("#initial_video_placeholder").removeClass("hide");
		$("#no_video").val(1);

	}

	function deleteFile(path, id, type){

		var form_data = new FormData();

		form_data.append("path", path);
		form_data.append("file_id", id);
		form_data.append("file_type", type);
		form_data.append('form_id', $('#frm_id').val() );

		var url = window.location.origin + "/app/electronicinvitation/deleteFile";

		$.ajax({
			url:url,
			method:"POST",
			data: form_data,
			contentType: false,
			cache: false,
			dataType: "text",
			processData: false,
			beforeSend:function(){
				//$('#loader-show').addClass("loader");
			},
			success:function(data)
			{
				console.log(data);
			}
		});
		
	}

    function readFile(input, callback) {

        if (input.files && input.files[0]) {

			var form_data = new FormData();
			form_data.append("file", input.files[0]);
			form_data.append('form_id',$('#frm_id').val());
			var url = window.location.origin + "/app/electronicinvitation/headerImageUpload";
			$.ajax({
				
				url:url,
				method:"POST",
				data: form_data,
				contentType: false,
				cache: false,
				dataType: "text",
				processData: false,
				beforeSend:function(){
					$(".lds-ellipsis, #uploading_file_text").removeClass("hide");
					$("#confirm_choice_text").addClass("hide");
				},
				success:function(data)
				{
					console.log('video data=>',data);
					var formpath = "<?php echo $formingpath; ?>";

					if(data){
						// $("#header_logo").attr("src", formpath+data);
						// $("#chosen_header_logo_image_base_64").val(formpath+data);

						$("#my_logos .panel-body").append(`
							<div class = "select-image-item logos">
								<img class = "img-zoom append-logo" data-id = "1" src = "`+formpath+data+`">
							</div>
						`)

					}
				
				},
				complete: function(){
					$(".lds-ellipsis, #uploading_file_text").addClass("hide");
					$("#confirm_choice_text").removeClass("hide");
				}
			});
        }
    }

	function deleteLocalImage(delete_icon){

		var $this = $(delete_icon);

		var $image = $this.parent().find('img')
		var image_src = $image.attr("src");
		var holder_src = $("#image_holder").attr("src");
		var chosen_src = $("#chosen_background_image_base_64").val();

		if(image_src == updatedImage){

			$(".container").append(`
				<div class = "tutorial-start-text-box" style = "width: auto;"> 
				<i class="fas fa-info img-zoom" id = "info_message"></i>
				<h3> ${translate.info_text.attention_text[language]} </h3>
				<p> ${translate.button_text.cant_delete_image[language]}  </p> 
				<div class = "tutorial-button-wraper">
					<a class = "btn btn-multiplier" onclick = "clear_tutorial(false);" > ${translate.button_text.afirmative_text[language]} </a>
				</div>
				</div>`
			);

			return false;
		}
    
		var images = JSON.parse( localStorage.getItem("background_images") );
		var localstorage_id = $image.attr("data-localstorage-id");

		if(image_src == holder_src){
		$("#image_holder").attr("src", "");
		}

		images.splice(localstorage_id, 1);
		$this.parent().remove();
		localStorage.setItem("background_images", JSON.stringify(images) );

		return true;
  	}

	function loadLocalImages(){

		var images = JSON.parse( localStorage.getItem("background_images") );

		for(var i = 0; i < images.length; i++){
		
		/*$("#images_preview").append(`
			<div class = "select-image-item">
				<i class="fas fa-trash-alt delete-image-icon"></i>
				<img class = "img-zoom" data-localstorage-id = "${i}" src = "${images[i]}">
			</div>  
		`);*/

		}

		// $(".delete-image-icon").click( function(){
		// 	deleteLocalImage(this)
		// });

		$(".select-image-item img").click(function(){
			$("#image_holder").attr("src", $(this).attr("src") );
			$("#chosen_background_image_base_64").val( $(this).attr("src") );
		});
		
	}

	function loadLocalVideos(){
	
		$(".select-video-tag").click(function(){
			var videopath =  $(this).children("source").attr("src");

			var videoid =  $(this).children("source").attr("data-id");
			var video_360 =  $(this).children("source").attr("data-val");
		
			$("#video_holder_wrapper").html(`
				<video controls>
					<source id = "video_holder" src="`+video_360+`" type="video/mp4">
				</video>
			`)
			
			$("#chosen_video").val(video_360);
			$("#chosen_video_id").val(videoid);
		});

	}

	function createImageData64FromColor(color_hex){

		var canvas = document.getElementById("canvas");
		var ctx = canvas.getContext("2d");
		ctx.fillStyle = color_hex;
		ctx.fillRect(0, 0, canvas.width, canvas.height);

		var base64 = canvas.toDataURL();

		return base64;
	}

  	// Croppie

	var croppie_width = $("#image_holder").css("width");
	var croppie_height = $("#image_holder").css("height") - 50;

	// croppie = new Croppie($("#image_holder_wrapper").get(0), {
	// 	viewport: {
	// 		width: croppie_width,
	// 		height: croppie_height,
	// 		type: 'square',
	// 	},
	// 	boundary: {
	// 		width: croppie_width,
	// 		height: croppie_height,
	// 	},
	// 	enableOrientation: true
	// });

	$("#crop_image").click(function(){

		croppie.bind({
		url: $("#image_holder").attr("src"),
		});

		$("#right_images_controls a").toggleClass("hide");
		$("#image_holder_wrapper div").removeClass("hide");
		$("#image_holder").addClass("hide");

	});

	$('.vanilla-rotate').on('click', function(ev) {
		croppie.rotate(parseInt($(this).data('deg')));
	}); 

  	$("#image_holder_wrapper").get(0).addEventListener('update', function (ev) {
      	croppie.result({type:'base64'}).then(function(resp) {
          croped_result = resp;
    	});
    });

	$("#crop_image_confirm").click(function(){

		$("#image_holder").attr("src", croped_result);
		$("#chosen_image_base_64").val('');

		addImage(croped_result);

		$("#right_images_controls a").toggleClass("hide");
		$("#image_holder").removeClass("hide");
		$("#image_holder_wrapper div").addClass("hide");

	});

	$("#crop_image_cancel").click(function(){

		$("#right_images_controls a").toggleClass("hide");
		$("#image_holder").removeClass("hide");
		$("#image_holder_wrapper div").addClass("hide");

	});	

  	// JS FOR THE TUTORIAL

  	var tutorialFlag = "<?php echo $formInfo->tutorial_watch; ?>";

	if(tutorialFlag == 0){
		
		$(".container").append(`
			<div class = "tutorial-start-text-box"> 
				<h3> <?= __(tutorialStartTitle) ?> </h3>
				<p> <?= __(tutorialStartText) ?> </p> 
				<div class = "tutorial-button-wraper">
					<a class = "btn btn-multiplier btn-green" onclick = "clear_tutorial(false); start_tutorial();" > <?= __(yes) ?> </a>
					<a class = "btn btn-multiplier" onclick = "clear_tutorial()" > <?= __(no) ?> </a>
				</div>
			</div>`
	);
	}
	else{
		clear_tutorial();
	}

	function start_tutorial(){

		var form_data = new FormData();
		form_data.append('form_id',$('#frm_id').val());
		var url = window.location.origin + "/app/electronicinvitation/chnageTutorialSeenFlag";
		$.ajax({
			url:url,
			method:"POST",
			data: form_data,
			contentType: false,
			cache: false,
			dataType: "text",
			processData: false,
			beforeSend:function(){
			//$('#loader-show').addClass("loader");
			},
			success:function(data)
			{
				console.log('video data=>',data);
				if(data){
					$(".tutorial-overlay").removeClass("hide");
					$(".shown").removeClass("shown");

					$(".container").append(`
						<div class = "tutorial-start-text-box" id = "tutorial_welcome_message"> 
							<h3> <?= __(tutorialStartTitle) ?> </h3>
							<p> <?= __(tutorialWelcomeText) ?> </p> 
							<div class = "tutorial-button-wraper">
								<a class = "btn btn-multiplier btn-green" onclick = "tutorial_next_step();" > <?= __(next) ?> </a>
							</div>
						</div>`
					);
				}
			}
		});
	}

	function tutorial_previous_step(){

		var $shown = $(".shown");
		var $this_shown = $( $shown.get( $shown.length - 1) );

		$this_shown.removeClass("shown");

		clear_tutorial(false);

		if( $shown.length === 1 ){
		start_tutorial();
		}
		else{

		var $last_shown = $( $shown.get( $shown.length - 2) );

		$last_shown.removeClass("shown");

		tutorial_show_item( $last_shown );
		}

	}

	function tutorial_next_step(){

		if( $(".tutorial-step").not(".shown").length > 0 ){
			clear_tutorial(false);
			tutorial_show_item( $( $(".tutorial-step").not(".shown").get(0) ) );
		}
		else{
			clear_tutorial();
		}

	}

	function tutorial_show_item($element){

		if( $element.attr("data-tutorial-show-parent") == "false"){
			$(".tutorial-overlay").addClass("tutorial-item-active");
		}
		else{
			$element.parent().addClass("tutorial-item-active");
		}
		
		$element.addClass("shown");

		$element.parent().append(`
			<div class = "tutorial-text-box" style = 'width: auto; min-width: 280px; max-width: 350px;'> 
				<h3> ${$element.attr("data-tutorial-name")} </h3>
				<p> ${$element.attr("data-tutorial-text")} </p> 
				<div class = "tutorial-button-wraper">
				<a class = "btn btn-multiplier" onclick = "tutorial_previous_step();" > <?= __(go_back) ?> </a>
					<a class = "btn btn-multiplier btn-green" onclick = "tutorial_next_step();" > <?= __(next) ?> </a>
				</div>
			</div>`
		);

		var scrollTo = ( $(window).width() > 1000) ? "center" : "start"; 
		console.log(scrollTo);
		console.log($element);
		$element.get(0).scrollIntoView({block: scrollTo, behavior: "smooth"});
	}

	function clear_tutorial(hide_overlay = true){

		if(hide_overlay){
			$(".tutorial-overlay").addClass("hide");
		}

		$(".tutorial-start-text-box").remove();
		$(".tutorial-text-box").remove();
		
		$(".tutorial-item-active").removeClass("tutorial-item-active");

	}

  	//

	function copyToClipboard(elem) {
		// create hidden text element, if it doesn't already exist
		var targetId = "_hiddenCopyText_";
		var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
		var origSelectionStart, origSelectionEnd;
		if (isInput) {
			// can just use the original source element for the selection and copy
			target = elem;
			origSelectionStart = elem.selectionStart;
			origSelectionEnd = elem.selectionEnd;
		} else {
			// must use a temporary form element for the selection and copy
			target = document.getElementById(targetId);
			if (!target) {
				var target = document.createElement("textarea");
				target.style.position = "absolute";
				target.style.left = "-9999px";
				target.style.top = "0";
				target.id = targetId;
				document.body.appendChild(target);
			}
			target.textContent = elem.textContent;
		}
		// select the content
		var currentFocus = document.activeElement;
		target.focus();
		target.setSelectionRange(0, target.value.length);

		// copy the selection
		var succeed;
		try {
			succeed = document.execCommand("copy");
		} catch(e) {
			succeed = false;
		}
		// restore original focus
		if (currentFocus && typeof currentFocus.focus === "function") {
			currentFocus.focus();
		}

		if (isInput) {
			// restore prior selection
			elem.setSelectionRange(origSelectionStart, origSelectionEnd);
		} else {
			// clear temporary content
			target.textContent = "";
		}
		return succeed;
	}

</script>

<script type="text/javascript">

  // PAGE SCRIPTS

    //DEV-116 - Get mandatory aditional data at registration forms
    function countyChanged(){
        var contry_flag = $("#countries_dropdown option:selected").attr("data-flag");
        $('#countryflag').attr('src', contry_flag);

        var country_code = $("#countries_dropdown option:selected").attr("data-phone");
        $('#countrycode').val(country_code);

        var country_id = $("#countries_dropdown option:selected").attr("data-contry-id");

        var states = $("#state");

        if (country_code == '55'){

            $('#phone').inputmask('(99) 99999-9999',{ "clearIncomplete": true });

        }else{
            $('#phone').inputmask('');
            }


    }

    function validateForm(){

        var name = $('#name').val();
        var email = $('#email').val();
        var country = $('#countries_dropdown').val();
        var phone = $('#phone').val();


        if (name == '' || email == '' || country == '' || !validateBRPhone(phone) || !validateEmail(email)){
            return false;
        }else{
            return true;
    }

    }

    function  validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    function validateBRPhone($phone) {
        var phoneReg = /(\([1-9][0-9]\)?|[1-9][0-9])\s?([9]{1})([0-9]{4})-?([0-9]{4})$/;
        return phoneReg.test( $phone );
    }

 	function fbShareOptionSelect(){
          if($("#share").val()==""){
              $("#shareOptionRequired").show();
                  return false;
          }
          $("#shareOptionRequired").hide();
          $('#social_step_1').hide();$('#social_step_2').show();
  	}
  
    var from_data = <?php echo json_encode($form_data) ?>;
    // console.log(from_data);
    var validation_fields = {};
        jQuery.each(from_data, function(i, field)
        {
           if( typeof field.required !== 'undefined' )
           {
                var jsonObj = {};
                if( typeof field.subtype !== 'undefined' )
                {   
                    if( field.subtype == 'email' )
                    {
                        jsonObj['email'] = true;
                    }

                    if( field.subtype == 'tel' )
                    {
                        jsonObj['number'] = '!0';
                        jsonObj['minlength'] = '5';
                        jsonObj['maxlength'] = '10';
                    }
                }
                jsonObj['required'] = true;
                validation_fields[field.name] = jsonObj;
            }
        });

        var flagObj = {};
        flagObj['required'] = true;
        validation_fields['flag'] = flagObj;


        var streamTitleObj = {};
        streamTitleObj['required'] = true;
        validation_fields['stream_title'] = streamTitleObj;


        var streamDescriptionObj = {};
        streamDescriptionObj['required'] = true;
        validation_fields['stream_description'] = streamDescriptionObj;
                
                // console.log(validation_fields);
    var FormValidation = function() {
    var e = function() {
   
    var formLang = $("#form_lang").val();
    //alert(formLang);

      var validMsg = '';
      if(formLang == 'Portuguese')
      {
        validMsg = "Os campos Título e Descrição do post não podem ficar em branco.";
      }
      else if(formLang == 'English')
      {
        validMsg = "Post Title and Description fields must not be blank.";
      }
      else if(formLang == 'Spanish')
      {
        validMsg = "Los campos Título y Descripción de la publicación no deben estar en blanco.";
      }
            var e = $("#form_sample_1"),
                r = $(".alert-danger", e),
                i = $(".alert-success", e);
            e.validate({
                errorElement: "span",
                errorClass: "help-block",
                errorPlacement: function(error, element) {
                    // console.log(element);
                    if( ( element.parent('.mt-radio').length ) || ( element.parent('.mt-checkbox').length) ) {
                        error.insertAfter(element.parent().parent());
                    } else {
                        error.insertAfter(element);
    }
        },
                focusInvalid: !1,
                ignore: "",
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
        },
                    stream_title: validMsg,
                    stream_description: validMsg,
                },
                rules: validation_fields,
                // ,rules: {
                //     name: {
                //         minlength: 2,
                //         required: !0
                //     },
                //     email: {
                //         required: !0,
                //         email: !0
                //     },
                // },
                invalidHandler: function(e, t) {
                    //i.hide(), r.show(), App.scrollTo(r, -200)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-error")
                },
                submitHandler: function(e) {
                    return true;
                   // i.show(), r.hide()
                }
    });


            jQuery('#flag').change(function(){
                if( jQuery(this).val() == 'timeline' )
                {
                    jQuery("#share").rules("add", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");

                }
                else if( jQuery(this).val() == 'page' )
                {
                    jQuery("#pages").rules("add", "required");
                    jQuery("#share").rules("remove", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");
                }
                else if( jQuery(this).val() == 'group' )
                {
                    jQuery("#groups").rules("add", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#share").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");
                }
                else if( jQuery(this).val() == 'event' ){
                    jQuery("#events").rules("add", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#share").rules("remove", "required");
                }
                else
                {

                }
            });

        };
    return {
        init: function() {
            e() // t(), r(), i()
        }
    }
	}();
  jQuery(document).ready(function() {


        //DEV-116 - Get mandatory aditional data at registration forms
        $('#countries_dropdown').val('br'); 
        $('#countries_dropdown').trigger('change'); 

    $('#change_user').click(function(event){
        event.preventDefault();
        var urlLogout = base_url + 'commingusers/fblogout/?id=';

        <?php  if($session->read( 'social.is_login' ) == "google"){ ?>
            urlLogout = base_url + 'commingusers/commonLogOut/?id=';
        <?php } ?>
        var logout_url =  $(this).attr('href');
        $.ajax({
            'url' : urlLogout + <?php echo $_GET['id'];?>,
            success: function(data){
               window.location.href = logout_url;
            }
        });

    });
    $('#flag').change(function(){
        var flag = $('#flag').val();
        if(flag == 'timeline'){
           
            var hide_share_option = '<?php echo $FormListingData['hide_share_option'];?>';
            if( hide_share_option == "0"){
               $('#show_timeline').show();
            }
            $('#stream_title').hide();
            $('#show_pages').hide();
            $('#show_events').hide();
            $('#show_groups').hide();
        } else if(flag == 'page') {
            $('#show_pages').show();
            $('#show_events').hide();
            $('#stream_title').hide();
            $('#show_timeline').hide();
            $('#show_groups').hide();
        } else if(flag == 'group') {
            $('#show_groups').show();
            $('#show_events').hide();
            $('#show_pages').hide();
            $('#stream_title').hide();
            $('#show_timeline').hide();
        }else if(flag == 'event'){
            $('#show_events').show();
            $('#show_groups').hide();
            $('#show_pages').hide();
            $('#stream_title').hide();
            $('#show_timeline').hide();
        }else{
            $('#show_pages').hide();
            $('#show_events').hide();
            $('#show_timeline').hide();
            $('#stream_title').hide();
            $('#show_groups').hide();
            return false;
        }
	});
	
    $('#get_values').click(function(){
        window.location.href = base_url + 'commingusers/fblogin?id=' + <?php echo $_GET['id'];?>; 
    });

    $('#get_values_gmail').click(function(){

		if (validateForm() == true){

			//Get values from form to session (AJAX)
			var name_value = $('#name').val();
			var email_value = $('#email').val();
			var country_value = $('#countries_dropdown option:selected').text();
			var phone_value = $('#phone').val();
			var state_value = $('#state option:selected').text();
			var city_value = $('#city option:selected').text();


			$.ajax({
				url: "/app/commingusers/saveFormSession",
				type: "POST",
				data: {name: name_value, email: email_value, country: country_value, phone: phone_value, state: state_value, city: city_value },
				dataType: "json"
			}).done(function(result) {
			});

			window.location.href = base_url + 'commingusers/glogin?id=' + <?php echo $_GET['id'];?>; 
			
		}
		else {

			var formLang = $("#form_lang").val();
			if(validateBRPhone(phone))
			{
				if(formLang == 'Portuguese')
			{
				var validMsg = "Todos os campos são de preenchimento obrigatório.";
			}
			else if(formLang == 'English')
			{
				var validMsg = "All fields are required.";
			}
			else if(formLang == 'Spanish')
			{
				var validMsg = "Todos los campos son obligatorios.";
			}
			alert(validMsg);

			}
			else
			{
			if(formLang == 'Portuguese')
			{
				var validMsg = "Número de telefone móvel inválido.";
			}
			else if(formLang == 'English')
			{
				var validMsg = "Invalid mobile phone number.";
			}
			else if(formLang == 'Spanish')
			{
				var validMsg = "Número de teléfono móvil no válido.";
			}
			$('#phone').blur();
			alert(validMsg);

			}
		}
  	});

	$('#get_values_facebook').click(function(){

		if (validateForm() == true){

			//Get values from form to session (AJAX)
			var name_value = $('#name').val();
			var email_value = $('#email').val();
			var country_value = $('#countries_dropdown option:selected').text();
			var phone_value = $('#phone').val();

			$.ajax({
				url: "/app/commingusers/saveFormSession",
				type: "POST",
				data: {name: name_value, email: email_value, country: country_value, phone: phone_value },
				dataType: "json"
			}).done(function(result) {

			});

			// $('.fb_login_box').show();
			// $('#col_social_text, #col_social_form, .flag_row, .img_row').hide();

			$("#share_modal").removeClass("hide");
			$('html, body').animate({ scrollTop: 0 }, 'slow');

		}
		else{

			if(validateBRPhone( $('#phone').val() )) {

				if(language == 'Portuguese'){
					var validMsg = "Todos os campos são de preenchimento obrigatório.";
				}
				else if(language == 'English'){
					var validMsg = "All fields are required.";
				}
				else if(language == 'Spanish'){
					var validMsg = "Todos los campos son obligatorios.";
				}

				alert(validMsg);
			}
			else{
				if(language == 'Portuguese'){
					var validMsg = "Número de telefone móvel inválido.";
				}
				else if(language == 'English'){
					var validMsg = "Invalid mobile phone number.";
				}
				else if(language == 'Spanish'){
					var validMsg = "Número de teléfono móvil no válido.";
				}

				alert(validMsg);
			}
		}
    });
   
  FormValidation.init();
   
  });

</script>

<script type="text/javascript">

	$(document).on('change', '.other', function (event) {
		if($(this).is(':checked')){
			$(".other-box").show(200);
			$(".other-box").prop('required',true);
			// $(this).parent().find(".help-block").show();
		}else{
			$(".other-box").hide(200);
			// $(this).parent().find(".help-block").hide();
			$(".other-box").prop('required',false);
		}
	});

	$(document).on('keyup', '.other-box', function (event) {
		$(".other").val($(this).val());
	});

	$(document).on("click", "input[class='radio-input']", function (event) 
	{
		var is_other = $(this).attr('other-field');

		if(is_other == '1'){
			$(".other-box-radio").show(200);
			$(".other-box-radio").prop('required',true);
			// $(this).parent().find(".help-block").show();
		}else{
			$(".other-box-radio").hide(200);
			// $(this).parent().find(".help-block").hide();
			$(".other-box-radio").prop('required',false);
	}

	});
	$(document).on('keyup', '.other-box-radio', function (event) {
		$(".radio-input").val($(this).val());
	});

</script>

<script>

	$(document).on('change', '.addvideogallary_video_input', function(e){
		//alert();
		var form_data = new FormData();
		form_data.append("file", document.getElementById('addvideogallary').files[0]);
		form_data.append('form_id',$('#frm_id').val());
		var url = window.location.origin + "/app/electronicinvitation/staticVideoUpload";
		$.ajax({	
			url:url,
			method:"POST",
			data: form_data,
			contentType: false,
			cache: false,
			dataType: "text",
			processData: false,
			beforeSend:function(){
				$(".lds-ellipsis, #uploading_file_text").removeClass("hide");
				$("#confirm_choice_text").addClass("hide");
			},
			success:function(dt)
			{
				console.log('video data=>',dt);
				data = JSON.parse(dt);
				if(data){
					var thumb_path = "<?php echo $dynamicvideopath; ?>"+data.videoName_thumb;
					var video_id = data.videoName_id;
					var video_360 = "<?php echo $dynamicvideopath; ?>"+data.videoName_360;

					$("#my_videos .panel-body").append(`
						<div class = "select-image-item videos">
							<video class = "img-zoom append-video">
								<source  src="`+thumb_path+`"  data-id="`+video_id+`"  data-val="`+video_360+`" type="video/mp4">
							</video>
						</div>
					`)
				}
			},
			complete: function(){
				$(".lds-ellipsis, #uploading_file_text").addClass("hide");
				$("#confirm_choice_text").removeClass("hide");
			}
			});
	});

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150918709-1"></script>

<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-150918709-1');
</script>

<style>
/** LOADER */

	.lds-ellipsis {
	display: inline-block;
	position: relative;
	width: 80px;
	height: 10px;
	}
	.lds-ellipsis div {
	position: absolute;
	top: 0px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: #fff;
	animation-timing-function: cubic-bezier(0, 1, 1, 0);
	}
	.lds-ellipsis div:nth-child(1) {
	left: 8px;
	animation: lds-ellipsis1 0.6s infinite;
	}
	.lds-ellipsis div:nth-child(2) {
	left: 8px;
	animation: lds-ellipsis2 0.6s infinite;
	}
	.lds-ellipsis div:nth-child(3) {
	left: 32px;
	animation: lds-ellipsis2 0.6s infinite;
	}
	.lds-ellipsis div:nth-child(4) {
	left: 56px;
	animation: lds-ellipsis3 0.6s infinite;
	}
	@keyframes lds-ellipsis1 {
	0% {
		transform: scale(0);
	}
	100% {
		transform: scale(1);
	}
	}
	@keyframes lds-ellipsis3 {
	0% {
		transform: scale(1);
	}
	100% {
		transform: scale(0);
	}
	}
	@keyframes lds-ellipsis2 {
	0% {
		transform: translate(0, 0);
	}
	100% {
		transform: translate(24px, 0);
	}
	}

</style>