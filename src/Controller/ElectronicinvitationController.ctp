<?php

namespace App\Controller;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Network\Email\Email;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Aws\S3\S3Client;


/**
 * TYPES
 * 1 -> Log Image
 * 2 -> Image
 * 3 -> Video
 * 4 -> Solid Color Image
 *
 * 
 */

class ElectronicinvitationController extends AppController
{
  public $helpers = ['Form'];
   public $paginate = [
         'limit' => 25,
         'order' => [
         'Userss.email' => 'asc'
         ]];
   //$this->loadComponent('Resize');
   //Function for check admin session
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
		
		 
    }
    public function initialize()
    {
		parent::initialize();
        $this->loadComponent('Paginator');
	}

	public function basicInfo($id=null)
    {

        $countriesListing = TableRegistry::get("countries");
        $countriesListingData = $countriesListing->find('all')->toArray();
        $this->set('countries', $countriesListingData);

        //print_r($this->request->data);die;
		$this->viewBuilder()->layout('admin_inner');
        // $this->viewBuilder()->layout('embed');
		$FormListingModel = TableRegistry::get("form_listings");
        $elecVideoModel = TableRegistry::get("electronic_videos");
        $elecVideoSelectedModel = TableRegistry::get("electronic_selected_video");
		$commingUsersModel = TableRegistry::get("CommingUsers");
		$this->set('page','electronicinvitation');
		$this->set('editor',true);

		if(!empty($_GET['id'])){

			$formId=$_GET['id'];
			$FormSingleData=$FormListingModel->find('all')->where(['form_id'=>$formId])->first();
			$selectedLanguage=$this->siteLanguage();
			$form_title_lang=isset($FormSingleData->form_title)?$FormSingleData->form_title:'';
			$form_description_lang=isset($FormSingleData->form_description)?$FormSingleData->form_description:'';
			if($selectedLanguage=='Portuguese'){
				$form_title_lang=isset($FormSingleData->form_title_pt)?$FormSingleData->form_title_pt:'';
				$form_description_lang=isset($FormSingleData->form_description_pt)?$FormSingleData->form_description_pt:'';
			}else if($selectedLanguage=='Spanish'){
				$form_title_lang=isset($FormSingleData->form_title_sp)?$FormSingleData->form_title_sp:'';
				$form_description_lang=isset($FormSingleData->form_description_sp)?$FormSingleData->form_description_sp:'';
			}

			//Videos
			$elecData = $elecVideoModel->find('all')->where(['form_id'=>$formId, 'type' => 3])->toArray();
			//Images
			$elecDataImage = $elecVideoModel->find('all')->where(['form_id'=>$formId, 'type' => 2])->toArray();
			//Logos
			$elecDataLogo = $elecVideoModel->find('all')->where(['form_id' => $formId, 'type' => 1])->toArray();

            $elecSelectedData=$elecVideoSelectedModel->find('all')->where(['form_id'=>$formId])->first();

			$elecStaticImageModel = TableRegistry::get("electronic_static_image");
			$elecStaticImageData=$elecStaticImageModel->find('all')->toArray();
	
			$elecStaticVideoModel = TableRegistry::get("electronic_static_video");
			$elecStaticVideoData=$elecStaticVideoModel->find('all')->toArray();
			
			// echo "<pre>";
			// var_dump($elecStaticImageData);
			// exit();
			
            //Change to pass all the new fields
			$this->isFormOwner($FormSingleData);
			$this->set('formInfo',$FormSingleData);
            $this->set('formId',$formId);
			$this->set('form_title_lang',$form_title_lang);
			$this->set('form_description_lang',$form_description_lang);
			$this->set('selectedLanguage',$selectedLanguage);
            $this->set('elecData',$elecData);
            $this->set('elecSelectedData',$elecSelectedData);
			$this->set('elecDataImage',$elecDataImage);
			$this->set('elecDataLogo', $elecDataLogo);
			$this->set('elecStaticImageData',$elecStaticImageData);
			$this->set('elecStaticVideoData',$elecStaticVideoData);
		}

		// Configure::write("debug", 1);

		// debug($FormSingleData);
		// exit();


		if(isset($this->request->data) && !empty($this->request->data) && !empty($this->request->data['form_id']))
		{

			$formId =  $this->request->data['form_id'];
			$FormListData = $FormListingModel->newEntity();

            $elecVideoSelectedModel = TableRegistry::get("electronic_selected_video");
            $elecSelectedData = $elecVideoSelectedModel->find('all')->where(['form_id'=>$formId])->first();

            $elecImageSelectedModel = TableRegistry::get("electronic_selected_image");
            $elecSelectedImageData = $elecImageSelectedModel->find('all')->where(['form_id'=>$formId])->first();

			if (is_dir(WWW_ROOT . '/img/uploads/formimg/') && is_writable(WWW_ROOT . 'img/uploads/formimg/')) 
			{
			
                $form_img1 = $this->request->data['chosen_background_image_base_64'];

                //Change so we can send the id of images alredy in the server and so we can send more than one image 
                // background_image, header_logo, social_image
				if($form_img1) {
					list($type, $data) = explode(';', $form_img1);
					list(, $data)      = explode(',', $data);
					$data = base64_decode($data);			
					$form_img1_new_name=time().'.png';
					file_put_contents(WWW_ROOT . 'img/uploads/formimg/'.$form_img1_new_name, $data);
					$FormListData->form_img1 = @$form_img1_new_name;
				}

                // $form_img2 = $this->request->data['chosen_header_logo_image_base_64'];

				// if($form_img2) {
				// 	list($type, $data) = explode(';', $form_img2);
				// 	list(, $data)      = explode(',', $data);
				// 	$data = base64_decode($data);			
				// 	$form_img2_new_name=time().'.png';
				// 	file_put_contents(WWW_ROOT . 'img/uploads/formimg/'.$form_img2_new_name, $data);
				// 	$FormListData->selected_header_image = @$form_img2_new_name;
				// }

				/*$form_img1 = $this->request->data['img1'];
				if( !empty( $form_img1['name'] ) ) {
                    $form_img1_info = pathinfo($form_img1['name']);
                    $form_img1_ext = $form_img1_info['extension'];
                    $form_img1_new_name = time() . rand(1, 999) . '.' . $form_img1_ext;
                    if ($form_img1_ext == 'jpg' || $form_img1_ext == 'jpeg' || $form_img1_ext == 'png' || $form_img1_ext == 'JPG' || $form_img1_ext == 'JPEG' || $form_img1_ext == 'PNG') {
                        if (!$k = move_uploaded_file($form_img1['tmp_name'], WWW_ROOT . 'img/uploads/formimg/' . $form_img1_new_name)) {
                            $this->Flash->error(__(image_updaload_error));
                            $this->redirect(['controller' => 'Electronicinvitation', 'action' => 'basic-info?id='.$formId]);
                            echo "<script>location.href = ".HTTP_ROOT."'electronicinvitation/basic-info?id='.$formId;</script>";
                            die(image_updaload_error);
                        }
                        $FormListData->form_img1 = @$form_img1_new_name;


                        $x1=$this->request->data['x1'];
                        $y1=$this->request->data['y1'];
                        $width=$this->request->data['w'];
                        $height=$this->request->data['h'];

                        if($x1!='' && $y1!='' && $width!='') {
                            $sTempFileName = WWW_ROOT . 'img/uploads/formimg/' . $form_img1_new_name;
                            // change file permission to 644
                            //@chmod($sTempFileName, 0644);

                            if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                                $aSize = getimagesize($sTempFileName); // try to obtain image info
                                if (!$aSize) {
                                    //@unlink($sTempFileName);
                                    //return;
                                }

                                // check for image type
                                switch ($aSize[2]) {
                                    case IMAGETYPE_JPEG:
                                        $sExt = '.jpg';

                                        // create a new image from file
                                        $vImg = @imagecreatefromjpeg($sTempFileName);
                                        break;
                                    case IMAGETYPE_GIF:
                                        $sExt = '.gif';

                                        // create a new image from file
                                        $vImg = @imagecreatefromgif($sTempFileName);
                                        break;
                                    case IMAGETYPE_PNG:
                                        $sExt = '.png';
                                        // create a new image from file
                                        $vImg = @imagecreatefrompng($sTempFileName);
                                        break;
                                    default:
                                        @unlink($sTempFileName);
                                    //return;
                                }

                                $targ_w = 750;
								$targ_h = 300;
                                // create a new true color image
                                $vDstImg = @imagecreatetruecolor($targ_w, $targ_h);
                                // copy and resize part of an image with resampling
                                imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$x1, (int)$y1, (int)$targ_w, (int)$targ_h, $width, $height);

                                // define a result image filename
                                $sResultFileName = $sTempFileName;
                                // output image to file
                                imagejpeg($vDstImg, $sResultFileName, 90);

                            }
                        }
                    } else {
                        $this->Flash->error(__(image_updaload_error));
                        $this->redirect(['controller' => 'Electronicinvitation', 'action' => 'basic-info?id='.$formId]);
                        echo "<script>location.href = ".HTTP_ROOT."'electronicinvitation/basic-info?id='.$formId;</script>";
                        die(image_upload_error);
                    }
                }*/
			}

            //Change so we can save the new data in the DB
    		if ($this->request->data['img_status1'] == '1') {
    			$FormListData->form_img1 = '';
    		}
            //There is no way to delete the background image now.
			if($this->request->data['is_image_deleted']==1){
				$FormListData->form_img1 = '';
			}

			if($this->request->data('no_video') == 1){

                $FormListData->selected_video = '';
                $FormListData->selected_video_360 = '';
                $FormListData->selected_video_720 = '';
                $FormListData->selected_video_thumb = '';

				if($elecSelectedData){
					$elecVideoSelectedModel->delete($elecSelectedData);
				}

				$elecSelectedData = NULL;
			}

            if($elecSelectedData){
                $FormListData->selected_video = $elecSelectedData->selected_video;
                $FormListData->selected_video_360 = $elecSelectedData->selected_video_360;
                $FormListData->selected_video_720 = $elecSelectedData->selected_video_720;
                $FormListData->selected_video_thumb = $elecSelectedData->selected_video_thumb;

                $fmData=$FormListingModel->find('all')->where(['form_id'=>$_GET['id']])->first();

                $pathinfo_360 = pathinfo($elecSelectedData->selected_video_360);
                $v_name_360 = $pathinfo_360['filename'];

                $pathinfo_720 = pathinfo($elecSelectedData->selected_video_720);
                $v_name_720 = $pathinfo_720['filename'];

                $aryVideo = array();
                array_push($aryVideo,$v_name_360);
                array_push($aryVideo,$v_name_720);

                $url = "http://209.126.105.148:8182/api/v1/getAdaptiveLinkVod";
                $postTrasArr = array();
                $postTrasArr['film_name'] = $fmData->wowza_source_name;
                $postTrasArr['directoryName'] = "user-videos";
                $postTrasArr['availableABR_videos'] = $aryVideo;
                $postParmsNew = json_encode($postTrasArr);
                $postTransResponse = $this->postCurl($url,$postParmsNew);
                $api_responcs = json_decode( $postTransResponse );

                $FormListData->video_playing_url = $api_responcs->playing_url;

            }

            if($elecSelectedImageData){
                $FormListData->selected_image = $elecSelectedImageData->selected_image;
            }

            // if(@$this->request->data['chosen_background_image_base_64']){
            //     $FormListData->selected_image = @$this->request->data['chosen_background_image_base_64'];
            // }
            
            if(@$this->request->data['chosen_header_logo_image_base_64']){
                $FormListData->selected_header_image = @$this->request->data['chosen_header_logo_image_base_64'];
            }
			
			
			$selectedLanguage=$this->siteLanguage();
			$FormListData->language =@$this->request->data['flag'];
			$FormListData->form_id =@$this->request->data['form_id'];
			if($selectedLanguage=='Portuguese'){
                if(@$this->request->data['headline'] != ""){
                    $FormListData->form_title_pt =@$this->request->data['headline'];
                }
                if(trim($this->request->data['main_text_html']) != ""){
                    $FormListData->form_description_pt = trim($this->request->data['main_text_html']);
                }
			}else if($selectedLanguage=='Spanish'){
                if(@$this->request->data['headline'] != ""){
                    $FormListData->form_title_sp =@$this->request->data['headline'];
                }
                if(trim($this->request->data['main_text_html']) != ""){
                    $FormListData->form_description_sp = trim($this->request->data['main_text_html']);
                }
			}else{	
                if(trim($this->request->data['main_text_html']) != ""){		
                    $FormListData->form_title =@$this->request->data['headline'];
                }
                if(@$this->request->data['main_text_html'] != ""){
                    $FormListData->form_description = trim($this->request->data['main_text_html']);
                }
			}
			
			// Configure::write("debug", 2);
			// debug($this->request->data);
			// debug($FormListData);
			// exit();

			$FormSingleData=$FormListingModel->find('all')->where(['form_id'=>$formId])->first();			
			if($FormListingModel->save($FormListData)){
				$this->Flash->success(__(updaload_success));
				$this->redirect(['controller' => 'Electronicinvitation', 'action' => 'basic-info?id='.$formId.'&submit='.true]);
			}		
		}

	}

    function isFormOwner($FormSingleData)
    {
		if(!empty($FormSingleData)){
			if (  ( $this->user('role') != 1 ) && ( $this->user('role') != 2 ) )
			{
				if ( ( $this->user('role') == '3' && $FormSingleData['owner_id'] != $this->user('id') )  OR  ( $FormSingleData['created_by'] != $this->user('parent_id')  && $this->user('role') == '4' ) )
				{
					$this->Flash->success(__(error_403));
					return $this->redirect(['controller' => 'formbuilder', 'action' => 'listing']);
				}
			}
		}
    }

    public function staticVideoUpload(){
        /*$uploadpath = WWW_ROOT.'/img/uploads/electronic_videos/';
        
       if($_FILES["file"]["name"] != ''){
       
            $filename = str_replace(' ', '',basename($_FILES['file']['name']));
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $filename = time().".".$ext;
            $alphanum = "abcdefghijkmnpqrstuvwxyz23456789";
            $codekey = substr(str_shuffle($alphanum), 0, 8);
            $filename = $codekey.$filename;
            $image_path_stored = $uploadpath.$filename;      

            
            $moved = move_uploaded_file($_FILES['file']['tmp_name'], $image_path_stored);

            if($moved) {

                $form_id = $_POST['form_id'];
                if($form_id){

                    $ElecModel = TableRegistry::get("electronic_videos");
                    $formData            = $ElecModel->newEntity();
                    $formData->form_id        = $form_id;
                    $formData->videoName = $filename;  
                    $formData->type = 3;          
                    $ElecModel->save($formData);

                    echo $filename;die; 
                }

                      
            } else {
              echo $_FILES["file"]["error"];die;
            }
        
        }*/

        if($_FILES["file"]["name"] != ''){
            $filename = str_replace(' ', '',basename($_FILES['file']['name']));
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $filename = time().".".$ext;
            $alphanum = "abcdefghijkmnpqrstuvwxyz23456789";
            $codekey = substr(str_shuffle($alphanum), 0, 8);
            $filename = $codekey.$filename;
            //$image_path_stored = $this->uploadpath.$filename;
            
            /*------------------------------------------*/
            $s3_region='sa-east-1';
            $s3_bucket='multiplierapp-form-data';
            $config = [
               's3' => [
                   'key' => 'AKIAUKMIEK7TCYNVUWPW',
                   'secret' => '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',
                   'bucket' => $s3_bucket
               ]
            ];
            $s3 = S3Client::factory([
                'credentials' => [
                   'key' => $config['s3']['key'],
                   'secret' => $config['s3']['secret']
                ],
            'region' => $s3_region,
            'version' => 'latest'
            ]);

            $obj=$s3->putObject([
                            'Bucket'       => $config['s3']['bucket'],
                            'Key'          => 'user-videos/'.$filename,
                            'SourceFile'   => $_FILES['file']['tmp_name'],
                            'ACL'          => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY'
                        ]);
            /*------------------------------------------*/
            

            $uploadPath = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO;

            $pathinfo = pathinfo($uploadPath.$filename);
            $fileext = $pathinfo['extension'];
            $video_name = $pathinfo['filename'];

            // Compress Video

            $filenameWithPath = $uploadPath.$filename;
            $newfilename_360 = $video_name.'_360.mp4';

            $newfilename_720 = $video_name.'_720.mp4';

            $newfilename_thumb = $video_name.'_thumb.jpg';

            $newFilenameWithPath_360 = $uploadPath.$newfilename_360;
            $newFilenameWithPath_720 = $uploadPath.$newfilename_720;
            $newFilenameWithPath_thumb = $uploadPath.$newfilename_thumb;

            $video_360 = 'ffmpeg -i '.$filenameWithPath.' -vf "scale=640:360:force_original_aspect_ratio=decrease,pad=640:360:(ow-iw)/2:(oh-ih)/2" -c:v libx264 -b:v 1.0M -c:a aac -b:a 128k  '.$newFilenameWithPath_360;

            exec($video_360);

            /*------------------upload 360p video------------------------*/

            $obj=$s3->putObject([
                            'Bucket'       => $config['s3']['bucket'],
                            'Key'          => 'user-videos/'.$newfilename_360,
                            'SourceFile'   => $_FILES['file']['tmp_name'],
                            'ACL'          => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY'
                        ]);
            /*------------------------------------------*/


            $video_720 = 'ffmpeg -i '.$filenameWithPath.' -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2"  -c:v libx264 -b:v 2.5M -c:a aac -b:a 128k   '.$newFilenameWithPath_720;

            exec($video_720);


             /*------------------upload 720p video------------------------*/

            $obj=$s3->putObject([
                            'Bucket'       => $config['s3']['bucket'],
                            'Key'          => 'user-videos/'.$newfilename_720,
                            'SourceFile'   => $_FILES['file']['tmp_name'],
                            'ACL'          => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY'
                        ]);
            /*------------------------------------------*/


           $video_thumb = 'ffmpeg -i '.$filenameWithPath.' -vframes 1 -an -s 400x222 -ss 3 '.$newFilenameWithPath_thumb;

            exec($video_thumb);


             /*------------------upload thumb of video------------------------*/

            $obj=$s3->putObject([
                            'Bucket'       => $config['s3']['bucket'],
                            'Key'          => 'user-videos/'.$newfilename_thumb,
                            'SourceFile'   => $_FILES['file']['tmp_name'],
                            'ACL'          => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY'
                        ]);
            /*------------------------------------------*/



            $form_id = $_POST['form_id'];
            if($form_id){

                $ElecModel = TableRegistry::get("electronic_videos");
                $formData            = $ElecModel->newEntity();
                $formData->form_id        = $form_id;
                $formData->videoName = $filename;  
                $formData->videoName_360 = $newfilename_360;
                $formData->videoName_720 = $newfilename_720;
                $formData->videoName_thumb = $newfilename_thumb;
                $formData->type = 3;          
                $res = $ElecModel->save($formData);

                $data = array(
                'videoName' => $filename,
                'videoName_360' => $newfilename_360,
                'videoName_720' => $newfilename_720,
                'videoName_thumb' => $newfilename_thumb,
                'videoName_id' => $res->id
                );

                echo json_encode($data);die; 
            }
            
        }

    }

    public function saveSelectedVideo(){
        $form_id = $_POST['form_id'];
        $selected_val = $_POST['selected_val'];
         $selected_val_id = $_POST['selected_val_id'];
         $word = 'system-videos';

        if(strpos($selected_val, $word) !== false){

          //If user select static video
          $ElecModel = TableRegistry::get("electronic_selected_video");

                if($form_id){

                    $elecSelectedData=$ElecModel->find('all')->where(['form_id'=>$form_id])->first();

                    if(count($elecSelectedData) == 1){
                        $elecSelectedData->selected_video = $selected_val;
                         $elecSelectedData->selected_video_360  = "";
                        $elecSelectedData->selected_video_720 = "";
                        $elecSelectedData->selected_video_thumb = "";
                            if($ElecModel->save($elecSelectedData)){
                                echo $selected_val;die;
                            }

                    }else{
                        $formData            = $ElecModel->newEntity();
                        $formData->form_id        = $form_id;
                        $formData->selected_video = $selected_val; 
                        $formData->selected_video_360 = "";
                        $formData->selected_video_720 = "";
                        $formData->selected_video_thumb = "";
                        $ElecModel->save($formData);

                        echo $selected_val;die;
                    }
                    
                    
                }else{
                    echo "";die;
                }      

        } else{

                // If user select uploaded video
                $ElecallModel = TableRegistry::get("electronic_videos");
                $elecAllData=$ElecallModel->find('all')->where(['form_id'=>$form_id, 'type' => 3, 'id' => $_POST['selected_val_id']])->first();
                if(count($elecAllData) > 0){

                $ElecModel = TableRegistry::get("electronic_selected_video");

                if($form_id){

                    $elecSelectedData=$ElecModel->find('all')->where(['form_id'=>$form_id])->first();

                    if(count($elecSelectedData) == 1){
                        $elecSelectedData->selected_video = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName;
                        $elecSelectedData->selected_video_360  = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName_360;
                        $elecSelectedData->selected_video_720 = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName_720;
                        $elecSelectedData->selected_video_thumb = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName_thumb;
                            if($ElecModel->save($elecSelectedData)){
                                echo S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName_360;die;
                            }

                    }else{
                        $formData            = $ElecModel->newEntity();
                        $formData->form_id        = $form_id;
                        $formData->selected_video = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName;  
                        $formData->selected_video_360 = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName_360;
                        $formData->selected_video_720 = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName_720;
                        $formData->selected_video_thumb = S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName_thumb;        
                        $ElecModel->save($formData);

                        echo S3_MEDIA_PATH_ELECTRONICS_USER_VIDEO.$elecAllData->videoName_360;die;
                    }
                    
                    
                }else{
                    echo "";die;
                }
            }
        }
    }

    public function headerImageUpload(){
        
       if($_FILES["file"]["name"] != ''){

            $filename = str_replace(' ', '',basename($_FILES['file']['name']));
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $filename = time().".".$ext;
            $alphanum = "abcdefghijkmnpqrstuvwxyz23456789";
            $codekey = substr(str_shuffle($alphanum), 0, 8);
            $filename = $codekey.$filename;
            
            /*------------------------------------------*/
            $s3_region='sa-east-1';
            $s3_bucket='multiplierapp-form-data';
            $config = [
               's3' => [
                   'key' => 'AKIAUKMIEK7TCYNVUWPW',
                   'secret' => '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',
                   'bucket' => $s3_bucket
               ]
            ];
            $s3 = S3Client::factory([
                'credentials' => [
                   'key' => $config['s3']['key'],
                   'secret' => $config['s3']['secret']
                ],
            'region' => $s3_region,
            'version' => 'latest'
            ]);

            $obj=$s3->putObject([
                            'Bucket'       => $config['s3']['bucket'],
                            'Key'          => 'user-images/'.$filename,
                            'SourceFile'   => $_FILES['file']['tmp_name'],
                            'ACL'          => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY'
                        ]);
            /*------------------------------------------*/

             $form_id = $_POST['form_id'];

            $ElecModel = TableRegistry::get("electronic_videos");
            $formData = $ElecModel->find('all')->where(['form_id'=>$form_id, 'type' => 1])->first();

            if($form_id){

                // if(count($formData) == 1){
                //     $formData->videoName = $filename;
                //         if($ElecModel->save($formData)){
                //             echo $filename;die;
                //         }

                // }else{
				$formData            = $ElecModel->newEntity();
				$formData->form_id        = $form_id;
				$formData->videoName = $filename;  
				$formData->type = 1;         
				$ElecModel->save($formData);

				echo $filename;die; 
                // }

                
                
            }
        
        }
    }

    function chnageTutorialSeenFlag(){
        $form_id = $_POST['form_id'];

        if($form_id){

            $ElecModel = TableRegistry::get("form_listings");
            $formData=$ElecModel->find('all')->where(['form_id'=>$form_id])->first();

            if($formData){

                if(count($formData) == 1){
                    $formData->tutorial_watch = 1;
                        if($ElecModel->save($formData)){
                            echo 1;die;
                        }

                }
            }else{
            echo 0;die; 
        }
        }else{
            echo 0;die; 
        }
    }

    public function backgroundImageUpload(){
        //$uploadpath = WWW_ROOT.'/img/uploads/formimg/';
     
       if($_FILES["file"]["name"] != ''){
       
            /*$filename = str_replace(' ', '',basename($_FILES['file']['name']));
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $filename = time().".".$ext;
            $alphanum = "abcdefghijkmnpqrstuvwxyz23456789";
            $codekey = substr(str_shuffle($alphanum), 0, 8);
            $filename = $codekey.$filename;
            $image_path_stored = $uploadpath.$filename;      

            
            $moved = move_uploaded_file($_FILES['file']['tmp_name'], $image_path_stored);

            if($moved) {

                $form_id = $_POST['form_id'];
                if($form_id){

                    $ElecModel = TableRegistry::get("electronic_videos");
                    $formData            = $ElecModel->newEntity();
                    $formData->form_id        = $form_id;
                    $formData->videoName = $filename;  
                    $formData->type = 2;          
                    $ElecModel->save($formData);

                    echo $filename;die; 
                }

                      
            } else {
              echo $_FILES["file"]["error"];die;
            }*/

            $filename = str_replace(' ', '',basename($_FILES['file']['name']));
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $filename = time().".".$ext;
            $alphanum = "abcdefghijkmnpqrstuvwxyz23456789";
            $codekey = substr(str_shuffle($alphanum), 0, 8);
            $filename = $codekey.$filename;
            //$image_path_stored = $this->uploadpath.$filename;
            
            /*------------------------------------------*/
            $s3_region='sa-east-1';
            $s3_bucket='multiplierapp-form-data';
            $config = [
               's3' => [
                   'key' => 'AKIAUKMIEK7TCYNVUWPW',
                   'secret' => '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',
                   'bucket' => $s3_bucket
               ]
            ];
            $s3 = S3Client::factory([
                'credentials' => [
                   'key' => $config['s3']['key'],
                   'secret' => $config['s3']['secret']
                ],
            'region' => $s3_region,
            'version' => 'latest'
            ]);

            $obj=$s3->putObject([
                            'Bucket'       => $config['s3']['bucket'],
                            'Key'          => 'user-images/'.$filename,
                            'SourceFile'   => $_FILES['file']['tmp_name'],
                            'ACL'          => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY'
                        ]);
            /*------------------------------------------*/

            $form_id = $_POST['form_id'];
            if($form_id){

                $ElecModel = TableRegistry::get("electronic_videos");
                $formData            = $ElecModel->newEntity();
                $formData->form_id        = $form_id;
                $formData->videoName = $filename;  
                $formData->type = 2;          
                $ElecModel->save($formData);

                echo $filename;die; 
            }
        
        }
    }

    public function solidColorImageUpload(){

		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

		$solid_image_base64 = $POST['solid_image_base64'];

		$alphanum = "abcdefghijkmnpqrstuvwxyz23456789";
		$codekey = substr(str_shuffle($alphanum), 0, 8);
		$filename = $codekey.time().".png";
		
		/*------------------------------------------*/
		$s3_region='sa-east-1';
		$s3_bucket='multiplierapp-form-data';

		$config = [
			's3' => [
				'key' => 'AKIAUKMIEK7TCYNVUWPW',
				'secret' => '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',
				'bucket' => $s3_bucket
			]
		];

		$s3 = S3Client::factory([
			'credentials' => [
				'key' => $config['s3']['key'],
				'secret' => $config['s3']['secret']
			],
			'region' => $s3_region,
			'version' => 'latest'
		]);
		
		$obj = $s3->putObject([
						'Bucket'			=> $config['s3']['bucket'],
						'Key'				=> 'user-images/'.$filename,
						'Body'				=> $solid_image_base64,
						'ContentEncoding'	=> 'base64',
						'ContentType'		=> 'image/png',
						'ACL'				=> 'public-read',
						'StorageClass'		=> 'REDUCED_REDUNDANCY'
					]);
		/*------------------------------------------*/
		echo($obj);die;
		$form_id = $_POST['form_id'];

		// if($form_id){

		// 	$ElecModel = TableRegistry::get("electronic_videos");
		// 	$formData            = $ElecModel->newEntity();
		// 	$formData->form_id        = $form_id;
		// 	$formData->videoName = $filename;  
		// 	$formData->type = 3;          
		// 	$ElecModel->save($formData);

		// 	echo $filename;die; 
		// }
        
    }

    public function saveSelectedImage(){

        $form_id = $_POST['form_id'];
        $selected_val = $_POST['selected_val'];

        $ElecModel = TableRegistry::get("electronic_selected_image");

        if($selected_val && $form_id){

            $elecSelectedData=$ElecModel->find('all')->where(['form_id'=>$form_id])->first();

            if(count($elecSelectedData) == 1){
                $elecSelectedData->selected_image = $selected_val;
                    if($ElecModel->save($elecSelectedData)){
                        echo $selected_val;die;
                    }

            }else{
                $formData            = $ElecModel->newEntity();
                $formData->form_id        = $form_id;
                $formData->selected_image = $selected_val;          
                $ElecModel->save($formData);

                echo $selected_val;die;
            }
            
            
        }else{
            echo "";die;
        }
    }

	public function deleteFile(){
	
		$form_id = $_POST['form_id'];
		$path = $_POST['path'];
		$file_id = $_POST['file_id'];
		$file_type = $_POST['file_type'];
		
		$deleted = false;

		$elecVideoModel = TableRegistry::get("electronic_videos");

		$deleted_file = $elecVideoModel->get($file_id);

		$paths = explode(",", $path);

		if($deleted_file){

			$elecVideoModel->delete($deleted_file);

            $s3_region='sa-east-1';
            $s3_bucket='multiplierapp-form-data';
            $config = [
               's3' => [
                   'key' => 'AKIAUKMIEK7TCYNVUWPW',
                   'secret' => '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',
                   'bucket' => $s3_bucket
               ]
			];
			
            $s3 = S3Client::factory([
                'credentials' => [
                   'key' => $config['s3']['key'],
                   'secret' => $config['s3']['secret']
                ],
				'region' => $s3_region,
				'version' => 'latest'
            ]);

			foreach($paths as $path){

				$obj = $s3->deleteObject([
					'Bucket'       => $config['s3']['bucket'],
					'Key'          => $path,
				]);

			}
				
			$deleted = true;
		}

		echo $deleted;
		exit();
	}

}